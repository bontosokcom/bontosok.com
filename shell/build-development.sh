#!/usr/bin/env bash

echo "START BUILD PROCESS"

echo "COPY ENV FILES"
cp ../build/env.development ../build/.env
cp ../api/env.development ../api/.env
cp ../spa/env.development ../spa/.env

echo "CREATE NECESSARY DIRECTORIES"
mkdir ../api/cache
chmod 750 ../api/cache
mkdir ../api/logs
chmod 750 ../api/logs
mkdir ../api/sessions
chmod 750 ../api/sessions

mkdir ../api/public/static/temp
chmod 750 ../api/public/static/temp
mkdir ../api/public/static/uploads
chmod 750 ../api/public/static/uploads

echo "RUN DOCKER COMPOSE DEVELOPMENT BUILD"
docker-compose -f ../build/docker-compose.development.yml --env-file ../build/.env up --build --force-recreate -d

echo "INSTALL PACKAGES WITH COMPOSER"
docker exec -it build_phalcon_1 sh -c 'cd /srv/www && composer install && ls ./vendor'

echo "CONTAINERS:"
docker ps

echo "SHOW DOCKER COMPOSE LOGS"
docker-compose -f ../build/docker-compose.development.yml logs -f
