#!/usr/bin/env bash

echo "START BUILD PROCESS"

echo "COPY ENV FILES"
cp ../build/env.production ../build/.env
cp ../api/env.production ../api/.env
cp ../spa/env.production ../spa/.env

echo "CREATE NECESSARY DIRECTORIES"
mkdir ../api/cache
chmod 750 ../api/cache
mkdir ../api/logs
chmod 750 ../api/logs
mkdir ../api/sessions
chmod 750 ../api/sessions

mkdir ../api/public/static/temp
chmod 750 ../api/public/static/temp
mkdir ../api/public/static/uploads
chmod 750 ../api/public/static/uploads

echo "RUN DOCKER COMPOSE PRODUCTION BUILD"
cd ../build
docker-compose -f docker-compose.production.yml up --build --force-recreate -d
