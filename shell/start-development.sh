#!/usr/bin/env bash

echo "START DEVELOPMENT ENVIRONMENT UNDER DOCKER"
cd ../build
docker-compose -f ../build/docker-compose.development.yml --env-file ../build/.env up --build --force-recreate
