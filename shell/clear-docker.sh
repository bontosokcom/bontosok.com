#!/usr/bin/env bash

echo "CLEAR DOCKER"

docker rm -f $(docker ps -a -q)
docker ps
docker rmi -f $(docker images -a -q)
docker images
