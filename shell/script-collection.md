### Docker:

#### Generate model:

**All models:**
    
    docker exec -it --user=root build_phalcon_1 phalcon all-models --namespace='Api\Models' --get-set --extends=AbstractModel --doc --force --abstract --annotate --directory=/srv/www --config=/srv/www/app/config/config.php

**Only one model:**    
    
    docker exec -it --user=root build_phalcon_1 phalcon model --name=<TABLE_NAME> --namespace='Api\Models' --get-set --extends=AbstractModel --doc --force --abstract --annotate --directory=/srv/www --config=/srv/www/app/config/config.php



#### Links:

Confirm registration:
    
    /confirm-registration?activation_token=<token>

Reset password

    /reset-password?reset_password_token=<token>
