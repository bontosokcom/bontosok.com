<?php
declare(strict_types=1);

namespace Api\Helpers;

use Api\Exceptions\HelperException;

class ArrayHelper
{
    /**
     * @param $array
     * @param $key
     * @return array
     * @throws HelperException
     */
    public static function indexingArray($array, $key)
    {
        if (!is_array($array) || !is_string($key)) {
            throw new HelperException('Invalid parameters: indexingArray()');
        }
        if (!isset($array) || empty($key)) {
            throw new HelperException("Parameters is missing: indexingArray()");
        }

        $tmp = [];
        foreach ($array as $item) {
            if (isset($tmp[$item[$key]])) {
                throw new HelperException('Item is multiple by key: indexingArray()');
            }
            $tmp[$item[$key]] = $item;
        }

        return $tmp;
    }

    /**
     * @param $array
     * @param $key
     * @return array
     * @throws HelperException
     */
    public static function collectArrayItem($array, $key)
    {
        if (!is_array($array) || !is_string($key)) {
            throw new HelperException('Invalid parameters: collectArrayItem()');
        }
        if (!isset($array) || empty($key)) {
            throw new HelperException("Parameters is missing: collectArrayItem()");
        }

        $tmp = [];
        foreach ($array as $item) {
            if (!in_array($item[$key], $tmp)) {
                $tmp[] = $item[$key];
            }
        }

        return $tmp;

    }
}
