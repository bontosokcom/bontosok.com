<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTP400BadRequestException as HTTP400BadRequestExceptionAlias;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Exceptions\ValidationException;
use Api\Helpers\ArrayHelper;
use Api\Models\DismantledCars;
use Api\Models\MyCars;
use Api\Models\Offers;
use Api\Models\PartSearches;
use Api\Models\Users;
use Api\Transactions\OfferRequestsStatusTransaction;
use Api\Transactions\OfferRequestsTransaction;
use Api\Transactions\OffersStatusTransaction;
use Api\Transactions\OffersTransaction;

class OffersController extends ControllerBase
{
    /**
     * @return array
     * @throws HTTP400BadRequestExceptionAlias
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     * @throws ValidationException
     */
    public function sendOfferAction()
    {
        if (empty($this->getParam('part_searches_id'))) {
            throw new HTTP400BadRequestExceptionAlias('The part search\'s id is missing');
        }
        if (empty($this->getParam('price'))) {
            throw new HTTP400BadRequestExceptionAlias('The offered price is missing');
        }
        if (empty($this->getParam('details'))) {
            throw new HTTP400BadRequestExceptionAlias('The offered details is missing');
        }

        $partSearch = PartSearches::findFirst([
            "id=:id:",
            'bind' => ['id' => $this->getParam('part_searches_id')]
        ]);
        if (!$partSearch) {
            throw new HTTP404NotFoundException('Part search not found');
        }

        $dismantledCar = DismantledCars::findFirst(["
            users_id=:users_id:
            AND status='active'
            AND deleted=0
            AND car_code=:car_code:
        ", 'bind' => [
            'users_id' => $this->getUsersId(),
            'car_code' => $partSearch->getCarCode()
        ]]);
        if (!$dismantledCar) {
            throw new HTTP404NotFoundException('Dismantled car not found');
        }

        $offer = new Offers();

        $offer->setUsersId($this->getUsersId());
        $offer->setDismantledCarsId($dismantledCar->getId());
        $offer->setPartSearchesId($partSearch->getId());
        $offer->setCarCode($partSearch->getCarCode());
        $offer->setStatus('active');
        $offer->setDetails($this->getParam('details'));
        $offer->setPrice($this->getParam('price'));

        if (OffersTransaction::create($offer)) {
            if ($offerRequestsId = OfferRequestsTransaction::insertOrUpdateOfferRequestsByPartSearchesId(
                $this->getParam('part_searches_id'),
                'progress'
            )) {

                $offer->setOfferRequestsId($offerRequestsId);

                if (OffersTransaction::update($offer)) {
                    $this->response->setStatusCode(200);
                    return [
                        'status' => 'success',
                        '$partSearch' => $partSearch,
                        '$dismantledCar' => $dismantledCar
                    ];
                }
            } else {
                $offer->delete();
            }
        }
    }

    public function getOffersAction()
    {
        $myCars = MyCars::find([
            "users_id=:users_id: AND status='active' AND deleted=0",
            'columns' => 'id, status, code, make, model, platform, year',
            'bind' => ['users_id' => $this->getUsersId()],
            'order' => 'make ASC, model ASC, platform ASC, year ASC'
        ]);
        if (count($myCars)) {
            $myCars = ArrayHelper::indexingArray($myCars->toArray(), 'id');
            $n = 0;
            foreach ($myCars as &$myCar) {
                $myCar['order'] = $n++;
            }
        }

        $partSearches = PartSearches::find([
            "users_id=:users_id: AND deleted=0",
            'bind' => ['users_id' => $this->getUsersId()],
            'order' => 'created_at DESC'
        ]);
        if (count($partSearches)) {
            $partSearches = ArrayHelper::indexingArray($partSearches->toArray(), 'id');
            $n = 0;
            foreach ($partSearches as &$partSearch) {
                $partSearch['order'] = $n++;
            }
        }

        $receivedOffers = Offers::findReceivedOffers();

        $offers = [];
        foreach ($receivedOffers as $offer) {
            if (!isset($offers[$offer['my_cars_id']])) {
                $offers[$offer['my_cars_id']] = [
                    'id' => $myCars[$offer['my_cars_id']]['id'],
                    'code' => $myCars[$offer['my_cars_id']]['code'],
                    'make' => $myCars[$offer['my_cars_id']]['make'],
                    'model' => $myCars[$offer['my_cars_id']]['model'],
                    'platform' => $myCars[$offer['my_cars_id']]['platform'],
                    'year' => $myCars[$offer['my_cars_id']]['year'],
                    'counter' => [
                        'new' => 0,
                        'archive' => 0,
                        'reject' => 0,
                        'progress' => 0,
                        'revoke' => 0
                    ]
                ];
                $offers[$offer['my_cars_id']]['partSearches'] = [];
            }
            if (!isset($offers[$offer['my_cars_id']]['partSearches'][$offer['part_searches_id']])) {
                $offers[$offer['my_cars_id']]['partSearches'][$offer['part_searches_id']] = [
                    'id' => $partSearches[$offer['part_searches_id']]['id'],
                    'carCode' => $partSearches[$offer['part_searches_id']]['car_code'],
                    'createdAt' => $partSearches[$offer['part_searches_id']]['created_at'],
                    'description' => $partSearches[$offer['part_searches_id']]['description'],
                    'images' => $partSearches[$offer['part_searches_id']]['images_json'],
                    'order' => $partSearches[$offer['part_searches_id']]['order'],
                    'title' => $partSearches[$offer['part_searches_id']]['title'],
                    'counter' => [
                        'new' => 0,
                        'archive' => 0,
                        'reject' => 0,
                        'progress' => 0,
                        'revoke' => 0
                    ]
                ];
                $offers[$offer['my_cars_id']]['partSearches'][$offer['part_searches_id']]['offers'] = [];
            }

            $progressStatus = $offer['offers_status'] ? $offer['offers_status'] : 'new';

            $offers[$offer['my_cars_id']]['counter'][$progressStatus]++;
            $offers[$offer['my_cars_id']]['partSearches'][$offer['part_searches_id']]['counter'][$progressStatus]++;

            $offers[$offer['my_cars_id']]['partSearches'][$offer['part_searches_id']]['offers'][] = [
                'id' => $offer['id'],
                'dismantledCarsId' => $offer['dismantled_cars_id'],
                'partSearchesId' => $offer['part_searches_id'],
                'offerRequestsStatusId' => $offer['offer_requests_status_id'],
                'progressStatus' => $progressStatus,
                'status' => $offer['status'],
                'details' => $offer['details'],
                'price' => $offer['price'],
                'images' => $offer['images'],
                'createdAt' => $offer['created_at']
            ];
        }

        return ['status' => 'success', 'offers' => $offers];
    }

    /**
     * @return array
     * @throws HTTP400BadRequestExceptionAlias
     * @throws HTTP404NotFoundException
     */
    public function getPhoneByOfferAction()
    {
        if (!$this->request->getQuery('offersId')) {
            throw new HTTP400BadRequestExceptionAlias('The offer\'s id is missing');
        }
        $offersId = (int)$this->request->getQuery('offersId');
        $offer = Offers::findFirst(["id={$offersId}"]);
        if (!$offer) {
            throw new HTTP404NotFoundException('Offer not found');
        }

        $user = Users::getFirstByDismantledCarsId($offer->getDismantledCarsId());
        if (!$user) {
            throw new HTTP404NotFoundException('User not found');
        }

        if (OffersStatusTransaction::insertOrUpdateReceivedOffersStatus(
            (int)$offer->getId(),
            (int)$offer->getPartSearchesId(),
            'progress'
        )) {
            $this->response->setStatusCode(200);
            return ['status' => 'success', 'user' => [
                'firstName' => $user['first_name'],
                'lastName' => $user['last_name'],
                'phone' => $user['phone'],
                'city' => $user['city']
            ]];
        }
    }
}
