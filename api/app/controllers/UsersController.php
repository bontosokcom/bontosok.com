<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTPException;
use Api\Exceptions\MailerException;
use Api\Models\Users;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Transactions\UsersTransaction;
use Api\Exceptions\HTTP400BadRequestException;
use Api\Exceptions\HTTP409ConflictException;
use Phalcon\Security\Exception;
use Phalcon\Security\Random;

class UsersController extends ControllerBase
{
    /**
     * @return array
     * @throws Exception
     * @throws HTTP400BadRequestException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     */
    public function registrationAction()
    {
        if (!$this->getParam('terms')) {
            throw new HTTP400BadRequestException('You must agree the Terms and Conditions');
        }
        if ($this->getParam('password') !== $this->getParam('passwordConfirm')) {
            throw new HTTP400BadRequestException('The password and the passwordConfirm fields must be equal');
        }

        $user = new Users();

        $user->setEmail($this->getParam('email'));
        $user->setPassword(!empty($this->getParam('password')) ? $this->security->hash($this->getParam('password')) : '');
        $user->setType($this->getParam('type') ? 'supply' : 'demand');

        $activationToken = (new Random())->uuid();
        $user->setActivationToken($activationToken);

        if (UsersTransaction::create($user) && $this->sendConfirmRegistrationMail($user->getEmail(), $activationToken)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @param $to
     * @param $activationToken
     * @return mixed
     */
    private function sendConfirmRegistrationMail($to, $activationToken)
    {
        return $this->mailer->sendMail($to,
            [
                'subject' => 'Regisztráció jóváhagyása',
                'content' => [
                    'confirm-registration',
                    [
                        'activationLink' => BASE_URL."/confirm-registration?activation_token={$activationToken}",
                        'activationLinkAlias' => 'Aktivációs link'
                    ]
                ]
            ]
        );
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function confirmRegistrationAction()
    {
        $activationToken = $this->request->get('activation_token');
        if (empty($activationToken)) {
            throw new HTTP400BadRequestException('Bad request: token is empty or invalid');
        }

        $user = Users::findFirstByActivationToken($activationToken);

        $user->setStatus('active');
        $user->setActivationToken(null);
        $user->setRegistratedAt(date('Y-m-d H:i:s'));

        if (UsersTransaction::update($user)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws Exception
     * @throws HTTP400BadRequestException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     * @throws MailerException
     */
    public function forgotPasswordAction()
    {
        if (empty($this->getParam('email'))) {
            throw new HTTP400BadRequestException('You must agree the Terms and Conditions');
        }

        $user = Users::findFirstActiveByEmail($this->getParam('email'));

        $resetPasswordToken = (new Random())->uuid();
        $user->setResetPasswordToken($resetPasswordToken);

        if (UsersTransaction::update($user) && $this->sendResetPasswordMail($user->getEmail(), $resetPasswordToken)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success', 'reset_password_token' => $resetPasswordToken];
        }
    }

    /**
     * @param $to
     * @param $resetPasswordToken
     * @return mixed
     * @throws MailerException
     */
    private function sendResetPasswordMail($to, $resetPasswordToken)
    {
        try {
            return $this->mailer->sendMail($to,
                [
                    'subject' => 'Jelszó módosítása',
                    'content' => [
                        'reset-password',
                        [
                            'passwordResetLink' => BASE_URL."/reset-password?reset_password_token={$resetPasswordToken}",
                            'passwordResetLinkAlias' => 'Jelszó módosítása link'
                        ]
                    ]
                ]
            );
        } catch (Exception $e) {
            throw new MailerException('Mail sending failed', 602);
        }
    }

    /**
     * @throws HTTP400BadRequestException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function resetPasswordAction()
    {
        if (empty($this->getParam('resetPasswordToken'))) {
            throw new HTTP400BadRequestException('Bad request: token is empty or invalid');
        }
        if ($this->getParam('password') !== $this->getParam('passwordConfirm')) {
            throw new HTTP400BadRequestException('The password and the passwordConfirm fields must be equal');
        }

        $user = Users::findFirstActiveByResetPasswordToken($this->getParam('resetPasswordToken'));

        $user->setPassword($this->security->hash($this->getParam('password')));
        $user->setResetPasswordToken(null);

        if (UsersTransaction::update($user)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP404NotFoundException
     */
    public function getEmailByResetPasswordTokenAction()
    {
        $resetPasswordToken = $this->request->get('reset_password_token');
        if (empty($resetPasswordToken)) {
            throw new HTTP400BadRequestException('Bad request: token is empty or invalid');
        }

        $user = Users::findFirstActiveByResetPasswordToken($resetPasswordToken);

        return ['status' => 'success', 'email' => $user->getEmail()];
    }

    /**
     * @return array
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     */
    public function getLoggedInUserAction()
    {
        $user = Users::findFirstActiveById($this->getUsersId());

        return [
            'status' => 'success',
            'user' => [
                'username'      => !empty($user->getUsername()) ? $user->getUsername() : '',
                'firstName'     => !empty($user->getFirstName()) ? $user->getFirstName() : '',
                'lastName'      => !empty($user->getLastName()) ? $user->getLastName() : '',
                'email'         => !empty($user->getEmail()) ? $user->getEmail() : '',
                'type'          => $user->getType(),
                'isFilled'      => $user->getIsFilled(),
                'zip'           => !empty($user->getZip()) ? $user->getZip() : '',
                'city'          => !empty($user->getCity()) ? $user->getCity() : '',
                'companyName'   => !empty($user->getCompanyName()) ? $user->getCompanyName() : '',
                'phone'         => !empty($user->getPhone()) ? $user->getPhone() : ''
            ]
        ];
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function updatePersonalAction()
    {
        if (empty($this->getParam('city'))) {
            throw new HTTP400BadRequestException('The city field is not exists or empty');
        }
        if (empty($this->getParam('companyName'))) {
            throw new HTTP400BadRequestException('The companyName field is not exists or empty');
        }
        if (empty($this->getParam('firstName'))) {
            throw new HTTP400BadRequestException('The firstName field is not exists or empty');
        }
        if (empty($this->getParam('lastName'))) {
            throw new HTTP400BadRequestException('The lastName field is not exists or empty');
        }
        if (empty($this->getParam('phone'))) {
            throw new HTTP400BadRequestException('The phone field is not exists or empty');
        }
        if (empty($this->getParam('username'))) {
            throw new HTTP400BadRequestException('The username field is not exists or empty');
        }
        if (empty($this->getParam('zip'))) {
            throw new HTTP400BadRequestException('The zip field is not exists or empty');
        }

        $user = Users::findFirstActiveById($this->getUsersId());

        $user->setCity($this->getParam('city'));
        $user->setCompanyName($this->getParam('companyName'));
        $user->setFirstName($this->getParam('firstName'));
        $user->setLastName($this->getParam('lastName'));
        $user->setPhone($this->getParam('phone'));
        $user->setUsername($this->getParam('username'));
        $user->setZip($this->getParam('zip'));

        if (UsersTransaction::update($user)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function changePasswordAction()
    {
        if ($this->getParam('password') !== $this->getParam('passwordConfirm')) {
            throw new HTTP400BadRequestException('The password and the passwordConfirm fields must be equal');
        }

        $user = Users::findFirstActiveById($this->getUsersId());

        $user->setPassword($this->security->hash($this->getJsonRawBody()->password));
        $user->setResetPasswordToken(null);

        if (UsersTransaction::update($user)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function changeEmailAction()
    {
        if (empty($this->getParam('email'))) {
            throw new HTTP400BadRequestException('The email field is not exists or empty');
        }

        $user = Users::findFirstActiveById($this->getUsersId());

        $user->setEmail($this->getParam('email'));

        if (UsersTransaction::update($user)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function changeTypeAction()
    {
        if (empty($this->getParam('type'))) {
            throw new HTTP400BadRequestException('The type field is not exists or empty');
        }

        $user = Users::findFirstActiveById($this->getUsersId());

        if (in_array($this->getParam('type'), ['demand', 'supply'])) {
            $user->setType($this->getParam('type'));
        }

        if (UsersTransaction::update($user)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function deleteAccountAction()
    {
        $user = Users::findFirstActiveById($this->getUsersId());

        if (UsersTransaction::delete($user)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }
}

