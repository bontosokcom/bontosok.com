<?php
declare(strict_types=1);

namespace Api\Controllers;

use Firebase\JWT\JWT;
use Api\Exceptions\HTTP400BadRequestException;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTPException;
use Api\Models\Users;

class AuthController extends ControllerBase
{
    /**
     * @throws HTTP404NotFoundException | HTTP400BadRequestException |HTTP401UnauthorizedException
     */
    public function loginAction()
    {
        if (empty($this->getParam('email')) || empty($this->getParam('password'))) {
            throw new HTTP400BadRequestException('Email or Password is empty');
        }

        /** @var Users $user */
        $user = Users::findFirstActiveByEmail($this->getParam('email'));
        if (!$user) {
            throw new HTTP404NotFoundException('User Not Found');
        }

        $check = $this->security->checkHash($this->getParam('password'), $user->getPassword());
        if (!$check) {
            throw new HTTP401UnauthorizedException('Authentication failed: Invalid Password');
        }

        $payload = (object)[
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'type' => $user->getType(),
            'role' => 'Authorized',
            'exp' => time() + 86400
        ];

        $token = JWT::encode($payload, getenv('JWT_KEY'), 'HS256');

        $this->session->set('token',                    $token);
        $this->session->set('id',                       $payload->id);
        $this->session->set('type',                     $payload->type);
        $this->session->set('registration_date',        $payload->type);

        $this->cookies->set(
            'token',
            $token,
            time() + 86400
        );

        return ['token' => $token];
    }

    /**
     * @throws HTTP400BadRequestException
     * @throws HTTPException
     */
    public function logoutAction()
    {
        $authorization = $this->request->getHeader('Authorization');
        $bearerToken = preg_replace("/Bearer /", "", $authorization);
        $sessionToken = $this->session->get('token');

        if (!$sessionToken) {
            throw new HTTP400BadRequestException('The Token in the Authentication header is invalid');
        }

        if ($bearerToken === $sessionToken) {
            $this->session->destroy();
            $this->session->regenerateId();
        }

        if (!empty($this->session->get('token'))) {
            throw new HTTPException('Not Acceptable', 406);
        }

        $this->response->setStatusCode(200);
    }
}
