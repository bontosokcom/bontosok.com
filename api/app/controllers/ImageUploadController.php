<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTP400BadRequestException;
use Phalcon\Http\Request\File;
use Phalcon\Image\Adapter\Gd;
use Phalcon\Validation;
use Phalcon\Validation\Validator\File as FileValidator;

class ImageUploadController extends ControllerBase
{
    private $uploadTime;

    /**
     * @return array
     * @throws HTTP400BadRequestException
     */
    public function uploadImageAction()
    {
        $this->uploadTime = date('YmdHis');

        if ($this->request->hasFiles()) {
            $fileUrls = [];
            $files = $this->request->getUploadedFiles();
            foreach ($files as $file) {
                if ($this->validation($file)) {
                    $tempOriginalPath = STATIC_TEMP_PATH . DS . $this->clearFilename($file->getName());
                    if ($file->moveTo(
                        $tempOriginalPath
                    )) {
                        if (is_file($tempOriginalPath)) {
                            $fileUrls[] = [
                                'filename' => $this->clearFilename($file->getName()),
                                'original' => TEMP_URL . DS . $this->clearFilename($file->getName()),
                                'thumbnail' => $this->createThumbnail($tempOriginalPath, 250, 250)
                                    ? TEMP_URL . DS . $this->thumbFilename($this->clearFilename($file->getName()), 250, 250)
                                    : ''
                            ];
                        }
                    }
                }
            }
            return ['status' => 'success', 'tempImageUrls' => $fileUrls];
        } else {
            throw new HTTP400BadRequestException('File is empty');
        }
    }

    /**
     * @param File $file
     * @return bool
     * @throws HTTP400BadRequestException
     */
    private function validation($file)
    {
        $fileData['file'] = [
            'name'     => $file->getName(),
            'type'     => $file->getType(),
            'tmp_name' => $file->getTempName(),
            'error'    => $file->getError(),
            'size'     => $file->getSize()
        ];

        $validation = new Validation();
        $fileValidator = new FileValidator([
            "maxSize"              => "5M",
            "messageSize"          => ":field exceeds the max filesize (:max)",
            "allowedTypes"         => [
                "image/png",
                "image/jpeg"
            ],
            "messageType"          => "Allowed file types are :types",
            "minResolution"        => "600x450",
            "messageMinResolution" => "Min resolution of :field is :resolution"
        ]);

        $validation->add('file', $fileValidator);
        $messages = $validation->validate($fileData);

        if (count($messages)) {
            $data['message'] = [];
            foreach ($messages as $message) {
                $data['message'][] = $message->getMessage();
            }
            throw new HTTP400BadRequestException(implode(", ", $data['message']));
        }

        return true;
    }

    private function clearFilename($filename)
    {
        return $this->uploadTime . "-" . base64_encode(pathinfo($filename, PATHINFO_FILENAME)) . '.' . pathinfo($filename, PATHINFO_EXTENSION);
    }

    private function thumbFilename($filename, $width = 250, $height = 250)
    {
        return pathinfo($filename, PATHINFO_FILENAME) . "-{$width}x{$height}-thumb." . pathinfo($filename, PATHINFO_EXTENSION);
    }

    private function createThumbnail($path, $width = 250, $height = 250)
    {
        $tempThumbnailPath = STATIC_TEMP_PATH . DS . $this->thumbFilename($path, 250, 250);
        return $this->cropImage($path, $tempThumbnailPath, $width, $height);
    }

    private function cropImage($path, $savePath, $width, $height)
    {
        $image = new Gd($path);

        $offsetX = (int)round(($image->getWidth() - $width) / 2, 0);
        $offsetY = (int)round(($image->getHeight() - $height) / 2, 0);

        $image->crop($width, $height, $offsetX, $offsetY);

        return $image->save($savePath);
    }

}
