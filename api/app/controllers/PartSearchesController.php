<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Models\MyCars;
use Api\Models\PartSearches;
use Api\Transactions\PartSearchesTransaction;
use Api\Exceptions\HTTPException;
use Api\Exceptions\HTTP400BadRequestException;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;

class PartSearchesController extends ControllerBase
{
    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     */
    public function startPartSearchAction()
    {
        if (empty($this->getParam('myCarId'))) {
            throw new HTTP400BadRequestException('The car id is missing');
        }
        if (empty($this->getParam('title'))) {
            throw new HTTP400BadRequestException('The part search title is missing');
        }
        if (empty($this->getParam('description'))) {
            throw new HTTP400BadRequestException('The part search description is missing');
        }
//        if (!$this->getParam('images')) {
//            throw new HTTP400BadRequestException('The images param is missing');
//        }

        $myCar = MyCars::findFirst(["
            id=:id: AND users_id=:users_id:
        ", 'bind' => [
            'id' => $this->getParam('myCarId'),
            'users_id' => $this->getUsersId()
        ]]);

        if (!$myCar) {
            throw new HTTP404NotFoundException('The car not found');
        }

        $partSearch = new PartSearches();

        $partSearch->setUsersId($this->getUsersId());
        $partSearch->setMyCarsId($myCar->getId());
        $partSearch->setStatus('active');
        $partSearch->setCarCode($myCar->getCarCode());
        $partSearch->setTitle($this->getParam('title'));
        $partSearch->setDescription($this->getParam('description'));

        $saved = PartSearchesTransaction::create($partSearch);

        if ($saved && !empty($this->getParam('images'))) {
//            $imagesJson = $this->saveImagesForPartSearch([
//                'part_searches_id' => $saved,
//                'car_code' => $partSearch->getCarCode()
//            ]);
            if (!empty($imagesJson)) {
                $partSearch->setImagesJson($imagesJson);
                if (PartSearchesTransaction::update($partSearch)) {
                    $this->response->setStatusCode(200);
                    return ['status' => 'success'];
                }
            }
        } else {
            if ($saved) {
                $this->response->setStatusCode(200);
                return ['status' => 'success'];
            }
        }
    }

    /**
     * @return array
     * @throws HTTP401UnauthorizedException
     */
    public function getPartSearchesAction()
    {
        $query = [
            "users_id=:users_id: AND deleted=0",
            'bind' => ['users_id' => $this->getUsersId()],
            'order' => 'status ASC, created_at DESC'
        ];

        if ($myCarsId = $this->request->getQuery('my_cars_id')) {
            $query[0] .= ' AND my_cars_id=:my_cars_id:';
            $query['bind']['my_cars_id'] = $myCarsId;
        }

        $partSearches = PartSearches::find($query);

        $partSearchesArr = [];
        foreach ($partSearches as $partSearch) {
            $partSearchesArr[] = [
                'id' => $partSearch->getId(),
                'myCarsId' => $partSearch->getMyCarsId(),
                'status' => $partSearch->getStatus(),
                'carCode' => $partSearch->getCarCode(),
                'title' => $partSearch->getTitle(),
                'description' => $partSearch->getDescription(),
//                'images' => $this->imagesToArray($partSearch->getImagesJson())
                'createdAt' => $partSearch->getCreatedAt(),
            ];
        }

        $this->response->setStatusCode(200);
        return ['status' => 'success', 'part_searches' => $partSearchesArr];
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function setPartSearchStatusAction()
    {
        if (empty($this->getParam('id'))) {
            throw new HTTP400BadRequestException('The part search\'s id is missing');
        }
        if (empty($this->getParam('status'))) {
            throw new HTTP400BadRequestException('The part search\'s status is missing');
        }

        $partSearch = PartSearches::findFirst(["
            id=:id: AND users_id=:users_id:
        ", 'bind' => [
            'id' => $this->getParam('id'),
            'users_id' => $this->getUsersId()
        ]]);

        if (!$partSearch) {
            throw new HTTP404NotFoundException('The part search not found');
        }

        $partSearch->setStatus($this->getParam('status'));

        if (PartSearchesTransaction::update($partSearch)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function deletePartSearchAction()
    {
        if (empty($this->request->getQuery('id'))) {
            throw new HTTP400BadRequestException('The part search\'s id is missing');
        }

        $partSearch = PartSearches::findFirst(["
            id=:id: AND users_id=:users_id:
        ", 'bind' => [
            'id' => $this->request->getQuery('id'),
            'users_id' => $this->getUsersId()
        ]]);

        if (!$partSearch) {
            throw new HTTP404NotFoundException('The part search not found');
        }

        $partSearch->setStatus('inactive');

        if (PartSearchesTransaction::delete($partSearch)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }
}
