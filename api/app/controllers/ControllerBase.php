<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTP400BadRequestException;
use Api\Exceptions\HTTP401UnauthorizedException;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    /**
     * @param $key
     * @return mixed
     * @throws HTTP400BadRequestException
     */
    public function getParam($key)
    {
        if (isset($this->getJsonRawBody()->$key)) {
            return $this->getJsonRawBody()->$key;
        } else {
            throw new HTTP400BadRequestException("The {$key} field is not exists");
        }
    }

    /**
     * @throws HTTP400BadRequestException
     */
    public function getJsonRawBody(): object
    {
        $rawBody = $this->request->getJsonRawBody();
        if (!$rawBody) {
            throw new HTTP400BadRequestException('Bad request: Raw body is empty or invalid');
        }
        return $rawBody;
    }

    /**
     * @throws HTTP401UnauthorizedException
     */
    public function getUsersId(): int
    {
        $usersId = $this->session->get('id');
        if (empty($usersId)) {
            throw new HTTP401UnauthorizedException('User is unauthorized');
        }
        return (int) $usersId;
    }
}
