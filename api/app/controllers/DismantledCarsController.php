<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTP400BadRequestException;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Models\DismantledCars;
use Api\Transactions\DismantledCarsTransaction;

class DismantledCarsController extends ControllerBase
{
    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     * @throws HTTP401UnauthorizedException
     */
    public function addDismantledCarAction()
    {
        if (empty($this->getParam('dismantledCarCode'))) {
            throw new HTTP400BadRequestException('The dismantled car code is missing');
        }

        $dismantledCar = new DismantledCars();

        $dismantledCar->setUsersId($this->getUsersId());
        $dismantledCar->setCarCode($this->getParam('dismantledCarCode'));
        $dismantledCar->setStatus('active');

        if (DismantledCarsTransaction::create($dismantledCar)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP401UnauthorizedException
     */
    public function getDismantledCarsAction()
    {
        $dismantledCars = DismantledCars::find([
            "users_id={$this->getUsersId()} AND deleted=0",
            'columns' => 'id, status, car_code as carCode, make, model, platform, year',
            'order' => 'status ASC, make ASC, model ASC, year ASC'
        ]);

        if (count($dismantledCars)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success', 'dismantled_cars' => $dismantledCars];
        }

        $this->response->setStatusCode(204);
        return ['status' => 'success'];
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function setDismantledCarsStatusAction()
    {
        if (empty($this->getParam('id'))) {
            throw new HTTP400BadRequestException('The dismantled car\'s id is missing');
        }
        if (empty($this->getParam('status'))) {
            throw new HTTP400BadRequestException('The dismantled car\'s status is missing');
        }

        $dismantledCar = DismantledCars::findFirst(["
            id=:id: AND users_id=:users_id:
        ", 'bind' => [
            'id' => $this->getParam('id'),
            'users_id' => $this->getUsersId()
        ]]);

        if (!$dismantledCar) {
            throw new HTTP404NotFoundException('The dismantled car not found');
        }

        $dismantledCar->setStatus($this->getParam('status'));

        if (DismantledCarsTransaction::update($dismantledCar)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function deleteDismantledCarAction()
    {
        if (empty($this->request->getQuery('id'))) {
            throw new HTTP400BadRequestException('The dismantled car\'s id is missing');
        }

        $dismantledCar = DismantledCars::findFirst(["
            id=:id: AND users_id=:users_id:
        ", 'bind' => [
            'id' => $this->request->getQuery('id'),
            'users_id' => $this->getUsersId()
        ]]);

        if (!$dismantledCar) {
            throw new HTTP404NotFoundException('The dismantled car not found');
        }

        $dismantledCar->setStatus('inactive');

        if (DismantledCarsTransaction::delete($dismantledCar)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }
}
