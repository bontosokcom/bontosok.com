<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;

class ErrorController extends ControllerBase
{
    /**
     * @throws HTTP404NotFoundException
     */
    public function show404Action()
    {
        throw new HTTP404NotFoundException();
    }

    /**
     * @throws HTTP401UnauthorizedException
     */
    public function show401Action()
    {
        throw new HTTP401UnauthorizedException();
    }
}

