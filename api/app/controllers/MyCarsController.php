<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTP400BadRequestException;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Models\MyCars;
use Api\Transactions\MyCarsTransaction;

class MyCarsController extends ControllerBase
{
    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     */
    public function addMyCarAction()
    {
        if (empty($this->getParam('myCarCode'))) {
            throw new HTTP400BadRequestException('The car code is missing');
        }

        $myCar = new MyCars();

        $myCar->setUsersId($this->getUsersId());
        $myCar->setCarCode($this->getParam('myCarCode'));
        $myCar->setStatus('active');

        if (MyCarsTransaction::create($myCar)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

    /**
     * @return array
     * @throws HTTP401UnauthorizedException
     */
    public function getMyCarsAction()
    {
        $myCars = MyCars::find([
            "
                users_id=:users_id:
                AND deleted=0
            ",
            'bind' => [
                'users_id' => $this->getUsersId()
            ],
            'columns' => 'id, status, car_code As carCode, make, model, platform, year',
            'order' => 'status ASC, make ASC, model ASC, year ASC'
        ]);

        if (count($myCars)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success', 'my_cars' => $myCars];
        }

        $this->response->setStatusCode(204);
        return ['status' => 'success'];
    }

    /**
     * @return array
     * @throws HTTP400BadRequestException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public function deleteMyCarAction()
    {
        if (empty($this->request->getQuery('id'))) {
            throw new HTTP400BadRequestException('The car\'s id is missing');
        }

        $myCar = MyCars::findFirst(["
            id=:id: AND users_id=:users_id:
        ", 'bind' => [
            'id' => $this->request->getQuery('id'),
            'users_id' => $this->getUsersId()
        ]]);

        if (!$myCar) {
            throw new HTTP404NotFoundException('The car not found');
        }

        $myCar->setStatus('inactive');

        if (MyCarsTransaction::delete($myCar)) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }
}
