<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HTTP400BadRequestException as HTTP400BadRequestExceptionAlias;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Exceptions\ValidationException;
use Api\Helpers\ArrayHelper;
use Api\Models\MyCars;
use Api\Models\Offers;
use Api\Models\PartSearches;
use Api\Models\ReceivedOffers;
use Api\Transactions\ReceivedOffersTransaction;

class ReceivedOffersController extends ControllerBase
{
    public function getReceivedOffersAction()
    {
        $myCars = MyCars::find([
            "
                users_id=:users_id:
                AND status='active'
                AND deleted=0          
            ",
            'bind' => [
                'users_id' => $this->getUsersId()
            ]
        ]);

        $partSearches = PartSearches::find([
            "
                users_id=:users_id:
                AND status='active'
                AND deleted=0
            ",
            'bind' => [
                'users_id' => $this->getUsersId()
            ]
        ]);
        $partSearchesIds = ArrayHelper::collectArrayItem($partSearches->toArray(), 'id');

        $receivedOffers = ReceivedOffers::find([
            "
                users_id=:users_id:
                AND deleted=0
                AND part_searches_id IN ({part_searches_ids:array})
            ",
            'bind' => [
                'users_id' => $this->getUsersId(),
                'part_searches_ids' => $partSearchesIds
            ]
        ]);
        $indexedReceivedOffers = ArrayHelper::indexingArray($receivedOffers->toArray(), 'offers_id');

        $offers = Offers::find([
            "
                users_id!=:users_id:
                AND status='active'
                AND deleted=0
                AND part_searches_id IN ({part_searches_ids:array})
            ",
            'bind' => [
                'users_id' => $this->getUsersId(),
                'part_searches_ids' => $partSearchesIds
            ]
        ]);

        $response = [];
        foreach ($offers as $offer) {
            if (isset($indexedReceivedOffers[$offer->getId()])) {
                $status = $indexedReceivedOffers[$offer->getId()]['status'];
                $history = $indexedReceivedOffers[$offer->getId()]['history'];
            } else {
                $status = 'arrived';
                $history = '[]';
            }

            $response[] = [
                'carCode' => $offer->getCarCode(),
                'dismantledCarsId' => $offer->getDismantledCarsId(),
                'partSearchesId' => $offer->getPartSearchesId(),
                'offerRequestsId' => $offer->getOfferRequestsId(),
                'offersId' => $offer->getId(),
                'offer' => [
                    'id' => $offer->getId(),
                    'dismantledCarsId' => $offer->getDismantledCarsId(),
                    'partSearchesId' => $offer->getPartSearchesId(),
                    'offerRequestsId' => $offer->getOfferRequestsId(),
                    'carCode' => $offer->getCarCode(),
                    'status' => $offer->getStatus(),
                    'details' => $offer->getDetails(),
                    'price' => $offer->getPrice(),
                    'currency' => $offer->getCurrency(),
                    'images' => $offer->getImagesJson(),
                    'createdAt' => $offer->getCreatedAt(),
                ],
                'status' => $status,
                'history' => $history
            ];
        }

        $this->response->setStatusCode(200);
        return [
            'status' => 'success',
            'receivedOffers' => $response,
            '$myCars' => $myCars,
            '$partSearches' => $partSearches,
            '$partSearchesIds' => $partSearchesIds,
            '$receivedOffers' => $receivedOffers,
            '$offers' => $offers,
        ];

    }

    /**
     * @return array
     * @throws HTTP400BadRequestExceptionAlias
     * @throws HTTP404NotFoundException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     * @throws ValidationException
     */
    public function setReceivedOffersStatusAction()
    {
        if (empty($this->getParam('offers_id'))) {
            throw new HTTP400BadRequestExceptionAlias('The offer\'s id is missing');
        }
        if (empty($this->getParam('status'))) {
            throw new HTTP400BadRequestExceptionAlias('The offers\'s status is missing');
        }

        $offer = Offers::findFirst(["id={$this->getParam('offers_id')}"]);
        if (!$offer) {
            throw new HTTP404NotFoundException('Offer not found');
        }

        $partSearch = PartSearches::findFirst(["id={$offer->getPartSearchesId()}"]);
        if (!$partSearch) {
            throw new HTTP404NotFoundException('Part search not found');
        }

        if (ReceivedOffersTransaction::insertOrUpdateReceivedOffersStatus(
            (int)$offer->getId(),
            (int)$partSearch->getId(),
            $this->getParam('status')
        )) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }
}
