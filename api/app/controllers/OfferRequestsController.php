<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Exceptions\HelperException;
use Api\Exceptions\HTTP400BadRequestException as HTTP400BadRequestExceptionAlias;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Exceptions\ValidationException;
use Api\Helpers\ArrayHelper;
use Api\Models\DismantledCars;
use Api\Models\OfferRequests;
use Api\Models\PartSearches;
use Api\Transactions\OfferRequestsTransaction;

/**
 * @property mixed|null mailer
 */
class OfferRequestsController extends ControllerBase
{
    /**
     * @return array
     * @throws HTTP401UnauthorizedException
     * @throws HelperException
     */
    public function getOfferRequestsAction()
    {
        $offerRequests = OfferRequests::find([
            "
                users_id=:users_id:
                AND deleted=0
            ",
            'bind' => [
                'users_id' => $this->getUsersId()
            ]
        ]);
        $indexedOfferRequests = ArrayHelper::indexingArray($offerRequests->toArray(), 'part_searches_id');

        $dismantledCars = DismantledCars::find([
            "
                users_id=:users_id:
                AND status='active'
                AND deleted=0
            ",
            'bind' => [
                'users_id' => $this->getUsersId()
            ]
        ]);
        $indexedDismantledCars = ArrayHelper::indexingArray($dismantledCars->toArray(), 'car_code');
        $dismantledCarsCarCodes = ArrayHelper::collectArrayItem($dismantledCars->toArray(), 'car_code');

        $partSearches = PartSearches::find([
            "
                users_id!=:users_id:
                AND status='active'
                AND deleted=0
                AND car_code IN ({car_codes:array})
            ",
            'bind' => [
                'users_id' => $this->getUsersId(),
                'car_codes' => $dismantledCarsCarCodes
            ],
            'order' => 'created_at DESC, title ASC'
        ]);

        $response = [];
        foreach ($partSearches as $partSearch) {

            if (isset($indexedOfferRequests[$partSearch->getId()])) {
                $status = $indexedOfferRequests[$partSearch->getId()]['status'];
                $history = $indexedOfferRequests[$partSearch->getId()]['history'];
            } else {
                $status = 'arrived';
                $history = '[]';
            }

            $response[] = [
                'carCode' => $partSearch->getCarCode(),
                'dismantledCarsId' => $indexedDismantledCars[$partSearch->getCarCode()]['id'],
                'dismantledCars' => $indexedDismantledCars[$partSearch->getCarCode()],
                'partSearchesId' => $partSearch->getId(),
                'status' => $status,
                'history' => $history,
                'partSearch' => [
                    'id' => $partSearch->getId(),
                    'myCarsId' => $partSearch->getMyCarsId(),
                    'status' => $partSearch->getStatus(),
                    'carCode' => $partSearch->getCarCode(),
                    'title' => $partSearch->getTitle(),
                    'description' => $partSearch->getDescription(),
                    'images' => $partSearch->getImagesJson(),
                    'createdAt' => $partSearch->getCreatedAt()
                ],
            ];
        }

        $this->response->setStatusCode(200);
        return [
            'status' => 'success',
            'offerRequests' => $response
        ];
    }

    /**
     * @return array
     * @throws HTTP400BadRequestExceptionAlias
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     * @throws ValidationException
     *
     * Status options:
     * arrived: The first status after born the offer request.
     * archive: Now it is not relevant.
     * reject: If it is impossible.
     * progress: If the offer was sent.
     * revoke: If the offer was revoked after it has been already sent before.
     * done: If the deal is done.
     * close: If the deal isn't done.
     *
     */
    public function setOfferRequestsStatusAction()
    {
        if (empty($this->getParam('part_searches_id'))) {
            throw new HTTP400BadRequestExceptionAlias('The part search\'s id is missing');
        }
        if (empty($this->getParam('status'))) {
            throw new HTTP400BadRequestExceptionAlias('The part search\'s status is missing');
        }

        if (OfferRequestsTransaction::insertOrUpdateOfferRequestsByPartSearchesId(
            $this->getParam('part_searches_id'),
            $this->getParam('status')
        )) {
            $this->response->setStatusCode(200);
            return ['status' => 'success'];
        }
    }

}
