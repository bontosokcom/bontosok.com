<?php
declare(strict_types=1);

namespace Api\Controllers;

use Api\Models\CarCatalog;
use Api\Models\CarCatalogMake;
use Api\Models\CarCatalogModel;
use Api\Models\CarCatalogPlatform;
use Api\Models\CarCatalogYear;

class CarCatalogController extends ControllerBase
{
    public function catalogAction()
    {
        $tmp = [];

        $carCatalogMake = CarCatalogMake::find(["", "order" => "name ASC"]);
        foreach ($carCatalogMake as $make) {
            $tmp[] = (object)[
                'key' => 'MA'.$make->getId(),
                'parent' => '-',
                'name' => $make->getName()
            ];
        }

        $carCatalogModel = CarCatalogModel::find(["", "order" => "name ASC"]);
        foreach ($carCatalogModel as $model) {
            $tmp[] = (object)[
                'key' => 'MO'.$model->getId(),
                'parent' => 'MA'.$model->getCarCatalogMakeId(),
                'name' => $model->getName()
            ];
        }

        $indexedYears = [];
        $carCatalogYear = CarCatalogYear::find(["", "order" => "name ASC"]);
        foreach ($carCatalogYear as $year) {
            $indexedYears[$year->getCarCatalogPlatformId()] = [
                'id' => $year->getId(),
                'name' => $year->getName()
            ];
        }
        $carCatalogPlatform = CarCatalogPlatform::find(["", "order" => "name ASC"]);
        foreach ($carCatalogPlatform as $platform) {
            $tmp[] = (object)[
                'key' => "PL{$platform->getId()}_YE{$indexedYears[$platform->getId()]['id']}",
                'parent' => 'MO'.$platform->getCarCatalogModelId(),
                'name' => "{$platform->getName()} - {$indexedYears[$platform->getId()]['name']}"
            ];
        }

        return $tmp;
    }

    public function catalogMigrationAction()
    {
        return ['permission denied'];

        $carCatalog = CarCatalog::find(["", "order"=>"make ASC", "group"=>"make"]);
        foreach ($carCatalog as $item) {
            $make = new CarCatalogMake();
            $make->setName(trim($item->getMake(), " "));
            $make->save();
        }

        $carCatalogMake = CarCatalogMake::find();
        $carCatalogMakeIndexed = [];
        foreach ($carCatalogMake as $item) {
            $carCatalogMakeIndexed[trim($item->getName(), " ")] = $item->getId();
        }
        $carCatalog = CarCatalog::find(["", "order"=>"make ASC, model ASC"]);
        foreach ($carCatalog as $item) {
            $make = new CarCatalogModel();
            $make->setCarCatalogMakeId($carCatalogMakeIndexed[trim($item->getMake(), " ")]);
            $make->setName($item->getModel());
            try {
                $make->save();
            } catch (\PDOException $e) {
                var_dump($e->getMessage());
            }
        }


        $carCatalog = CarCatalog::find(["", "order"=>"make ASC, model ASC, platform ASC"]);
        foreach ($carCatalog as $item) {

            $cartCatalogModel = CarCatalogModel::findFirst("
                car_catalog_make_id = ".$carCatalogMakeIndexed[trim($item->getMake(), " ")]."
                AND name = '".$item->getModel()."'
            ");

            $make = new CarCatalogPlatform();
            $make->setCarCatalogMakeId($carCatalogMakeIndexed[trim($item->getMake(), " ")]);
            $make->setCarCatalogModelId($cartCatalogModel->getId());
            $make->setName(trim($item->getPlatform(), ", "));
            try {
                $make->save();
            } catch (\PDOException $e) {
                var_dump($e->getMessage());
            }
        }


        $carCatalog = CarCatalog::find(["", "order"=>"make ASC, model ASC, year ASC"]);
        foreach ($carCatalog as $item) {


            $cartCatalogModel = CarCatalogModel::findFirst("
                car_catalog_make_id = ".$carCatalogMakeIndexed[trim($item->getMake(), " ")]."
                AND name = '".$item->getModel()."'
            ");

            $cartCatalogPlatform = CarCatalogPlatform::findFirst("
                car_catalog_make_id = ".$carCatalogMakeIndexed[trim($item->getMake(), " ")]."
                AND car_catalog_model_id = ".$cartCatalogModel->getId()."
                AND name = '".trim($item->getPlatform(), ", ")."'
            ");

            $make = new CarCatalogYear();
            $make->setCarCatalogMakeId($carCatalogMakeIndexed[trim($item->getMake(), " ")]);
            $make->setCarCatalogModelId($cartCatalogModel->getId());
            $make->setCarCatalogPlatformId($cartCatalogPlatform->getId());
            $make->setName($item->getYear());
            try {
                $make->save();
            } catch (\PDOException $e) {
                var_dump($e->getMessage());
            }
        }

        return ["end"];
    }
}
