<?php
declare(strict_types=1);

namespace Api\Library\Mailer;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Api\Exceptions\MailerException;

class Manager
{
    /**
     * @var PHPMailer
     */
    private $mail;

    private $fromAddress = '';
    private $fromName = '';
    private $replayAddress = '';
    private $replayName = '';

    /**
     * Manager constructor.
     * @throws MailerException
     */
    public function __construct()
    {
        $this->fromAddress = getenv('MAIL_FROM_ADDRESS');
        $this->fromName = getenv('MAIL_FROM_NAME');
        $this->replayAddress = getenv('MAIL_REPLAY_TO_ADDRESS');
        $this->replayName = getenv('MAIL_REPLAY_TO_NAME');

        try {
            $this->initMailer();
        } catch (MailerException $e) {
            throw new MailerException($this->mail->ErrorInfo, 600);
        }
    }

    /**
     * @throws MailerException
     */
    public function initMailer()
    {
        $this->mail = new PHPMailer(true);

        try {
            $this->setServerSettings();
            $this->setContentSettings();
            $this->setSender();
        } catch (Exception $e) {
            throw new MailerException($this->mail->ErrorInfo, 600);
        }
    }

    private function setServerSettings()
    {
        $this->mail->SMTPDebug        = SMTP::DEBUG_OFF;
        $this->mail->isSMTP();
        $this->mail->Host             = getenv('SMTP_HOST');
        $this->mail->Port             = getenv('SMTP_PORT');
        $this->mail->SMTPAuth         = true;
        $this->mail->SMTPAutoTLS      = false;
        $this->mail->SMTPSecure       = false;
        $this->mail->Username         = getenv('SMTP_USER');
        $this->mail->Password         = getenv('SMTP_PASS');
    }

    private function setContentSettings()
    {
        $this->mail->CharSet = PHPMailer::CHARSET_UTF8;
        $this->mail->isHTML(true);
    }

    /**
     * @throws MailerException
     */
    private function setSender()
    {
        try {
            $this->mail->setFrom($this->fromAddress, $this->fromName);
            $this->mail->addReplyTo($this->replayAddress, $this->replayName);
            if (getenv('MAIL_BCC')) {
                $this->mail->addBCC(getenv('MAIL_BCC'));
            }
        } catch (Exception $e) {
            throw new MailerException($this->mail->ErrorInfo, 600);
        }
    }

    /**
     * @param string $recipient
     * @param array $config
     * @return bool
     * @throws MailerException
     */
    public function sendMail(string $recipient, array $config)
    {
        $this->validateConfig($recipient, $config);
        $this->parseRecipients($recipient);
        $this->parseContent($config);

        return $this->send();
    }

    /**
     * @throws MailerException
     */
    public function send()
    {
        try {
            return $this->mail->send();
        } catch (Exception $e) {
            throw new MailerException("Message could not be sent. Mailer Error: {$this->mail->ErrorInfo}", 602);
        }
    }

    /**
     * @param $recipient
     * @param $config
     * @throws MailerException
     */
    public function validateConfig($recipient, $config)
    {
        if (empty($recipient)) {
            throw new MailerException('Mailer validation error: Invalid recipient', 601);
        }
        if (empty($config) || !is_array($config)) {
            throw new MailerException('Mailer validation error: Invalid mailer config', 601);
        }
        if (!isset($config['subject']) || empty($config['subject'])) {
            throw new MailerException('Mailer validation error: Invalid subject', 601);
        }
        if (!isset($config['content']) || empty($config['subject']) || !is_array($config['content'])) {
            throw new MailerException('Mailer validation error: Invalid content config', 601);
        }
        if (!isset($config['content'][0]) || !is_string($config['content'][0])) {
            throw new MailerException('Mailer validation error: Invalid template name', 601);
        }
    }

    /**
     * @param $recipients
     * @throws MailerException
     */
    public function parseRecipients($recipients)
    {
        if (is_string($recipients)) {
            $this->setRecipient($recipients);
        } elseif (is_array($recipients)) {
            foreach ($recipients as $recipient) {
                if (isset($recipient[0]) && isset($recipient[1])) {
                    $this->setRecipient($recipient[0], $recipient[1]);
                } elseif (isset($recipient[0]) && !isset($recipient[1])) {
                    $this->setRecipient($recipient[0]);
                }
            }
        }
    }

    /**
     * @param $address
     * @param string $name
     * @throws MailerException
     */
    public function setRecipient($address, $name = '')
    {
        try {
            if (!empty($name)) {
                $this->mail->addAddress($address, $name);
            } else {
                $this->mail->addAddress($address);
            }
        } catch (Exception $e) {
            throw new MailerException($this->mail->ErrorInfo, 600);
        }
    }

    /**
     * @param array $config
     * @throws MailerException
     */
    public function parseContent(array $config)
    {
        $this->mail->Subject = $config['subject'];
        $this->mail->Body = (new Builder($config['content']))->render();
    }
}
