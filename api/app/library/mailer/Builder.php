<?php
declare(strict_types=1);

namespace Api\Library\Mailer;

use Api\Exceptions\MailerException;
use Phalcon\Di as DI;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\View;

class Builder
{
    private $templateDirectoryPath = 'templates/email/';
    private $templateName = '';
    private $params = [];

    /**
     * Builder constructor.
     * @param array $config
     * @throws MailerException
     */
    public function __construct(array $config)
    {
        $this->configure($config);
    }

    /**
     * @throws MailerException
     */
    public function render()
    {
        $mail = $this->getView()->getPartial($this->getTemplateName(), $this->params);
        if (!is_string($mail)) {
            throw new MailerException('Bad Mailer builder result', 605);
        }

        return $mail;
    }

    /**
     * @return string
     * @throws MailerException
     */
    private function getTemplateName()
    {
        $templatePath = $this->templateDirectoryPath . $this->templateName;

        if (!$this->getView()->exists($templatePath)) {
            throw new MailerException('Invalid mail template path or name', 652);
        }

        return $templatePath;
    }

    /**
     * @param $config
     * @throws MailerException
     */
    private function configure($config)
    {
        $this->validateConfig($config);
        $this->templateName = $config[0];
        $this->params = isset($config[1]) ? $config[1] : [];
    }

    /**
     * @param $config
     * @throws MailerException
     */
    private function validateConfig($config)
    {
        if (empty($config)) {
            throw new MailerException('Bad MailerBuilder config', 651);
        }
        if (!isset($config[0]) || empty($config[0]) || !is_string($config[0])) {
            throw new MailerException('Bad MailerBuilder config: Invalid template name', 651);
        }
    }

    /**
     * @return mixed
     */
    private function getView()
    {
        $view = $this->getDI()->getShared('view');
        if (!($view instanceof View)) {
            throw new \RuntimeException('A view object is required to access internal services');
        }

        return $view;
    }

    /**
     * @return mixed
     */
    private function getDI()
    {
        $di = DI::getDefault();
        if (!($di instanceof DiInterface)) {
            throw new \RuntimeException('A dependency injection object is required to access internal services');
        }

        return $di;
    }
}
