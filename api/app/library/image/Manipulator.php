<?php
declare(strict_types=1);

namespace Api\Library\Image;

use Api\Exceptions\ImageException;
use Phalcon\Image\Adapter\AdapterInterface;
use Phalcon\Image\Adapter\Gd;
use Phalcon\Image\Enum;

class Manipulator
{
    private $config;
    protected $newWidth = 600;
    protected $newHeight = 450;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return bool|mixed
     * @throws ImageException
     */
    public function create()
    {
        $this->calculateNewDimensions();

        if ($this->resizeImage()) {
            return $this->getTarget();
        }

        return false;
    }

    /**
     * @return AdapterInterface
     * @throws ImageException
     */
    private function resizeImage()
    {
        $image = new Gd($this->getOriginal());

        $image->resize(
            $this->newWidth,
            $this->newHeight,
            Enum::NONE
        );

        $offsetX = (int)round(($image->getWidth() - $this->getDimension('width')) / 2);
        $offsetY = (int)round(($image->getHeight() - $this->getDimension('height')) / 2);

        $image->crop($this->getDimension('width'), $this->getDimension('height'), $offsetX, $offsetY);

        return $image->save($this->getTarget());
    }

    /**
     * @throws ImageException
     */
    private function calculateNewDimensions()
    {
        $image = new Gd($this->config['original']);

        $multiplier = $this->getDimension('width') / $image->getWidth();
        $newWidth = $this->getDimension('width');
        $newHeight = $multiplier * $image->getHeight();

        if ($newHeight < $this->getDimension('height')) {
            $multiplier = $this->getDimension('height') / $image->getHeight();
            $newHeight = $this->getDimension('height');
            $newWidth = $multiplier * $image->getWidth();
        }

        $this->newWidth = (int)round($newWidth, 0);
        $this->newHeight = (int)round($newHeight, 0);
    }

    /**
     * @return mixed
     * @throws ImageException
     */
    private function getOriginal()
    {
        if (!isset($this->config['original']) || empty($this->config['original'])) {
            throw new ImageException('Original is missing', 672);
        }

        return $this->config['original'];
    }

    /**
     * @return mixed
     * @throws ImageException
     */
    private function getTarget()
    {
        if (!isset($this->config['target']) || empty($this->config['target'])) {
            throw new ImageException('Target is missing', 673);
        }

        return $this->config['target'];
    }

    /**
     * @param $side
     * @return bool|int
     * @throws ImageException
     */
    private function getDimension($side)
    {
        if (!isset($this->config['dimension']) || empty($this->config['dimension'])) {
            throw new ImageException('Dimension is missing', 674);
        }

        $exp = explode("x", $this->config['dimension']['dimension']);

        if ($side == 'width') return (int)$exp[0];
        if ($side == 'height') return (int)$exp[1];

        return false;
    }
}
