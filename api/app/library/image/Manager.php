<?php
declare(strict_types=1);

namespace Api\Library\Image;

use Api\Exceptions\ImageException;
use Phalcon\Di;

class Manager
{
    private $Di;
    protected $config;
    protected $original;
    protected $dimensions;
    protected $path;
    protected $url;
    protected $instances = [];

    /**
     * Manager constructor.
     * @param $original
     * @param $config
     * @throws ImageException
     */
    public function __construct($original, $config)
    {
        $this->path = STATIC_UPLOADS_PATH . DS . date('Ymd');
        $this->url = UPLOADS_URL . DS . date('Ymd');

        $this->config = $config;

        $this->Di = Di::getDefault();
        $this->setOriginal($original);
        $this->setDimensions();
    }

    /**
     * @return array
     * @throws ImageException
     */
    public function createInstances()
    {
        $this->instances = [];
        foreach ($this->dimensions as $name => $dimension) {
            $targetFilename = $this->getNewFilename(pathinfo($this->original, PATHINFO_FILENAME) . '.' . pathinfo($this->original, PATHINFO_EXTENSION), ['name' => $name, 'dimension' => $dimension]);
            $Manipulator = new Manipulator([
                'original' => $this->original,
                'target' => $this->path . DS . $targetFilename,
                'dimension' => ['name' => $name, 'dimension' => $dimension]
            ]);
            if (!is_dir($this->path)) {
                mkdir($this->path);
            }
            if ($Manipulator->create()) {
                $this->instances["{$name}-{$dimension}"] = $this->url . DS . $targetFilename;
            }
        }

        return $this->instances;
    }

    /**
     * @param $filename
     * @param $dimension
     * @return string
     */
    private function getNewFilename($filename, $dimension)
    {
        preg_match("/^[0-9]{12}/", $filename, $date);
        return trim(base64_encode("{$this->config['namePrefix']}-{$date[0]}-" . rand(0, 1000)), '=') . "-{$dimension['name']}-{$dimension['dimension']}" . "." . pathinfo($filename, PATHINFO_EXTENSION);
    }

    /**
     * @param $original
     * @throws ImageException
     */
    private function setOriginal($original)
    {
        if (empty($original)) {
            throw new ImageException('Original is missing', 670);
        }

        if (is_file($original)) {
            $this->original = $original;
        }
    }

    /**
     * @param bool $dimensions
     * @throws ImageException
     */
    private function setDimensions($dimensions = false)
    {
        if ($dimensions) {
            $this->dimensions = $dimensions;
        } else {
            $config = $this->Di->getConfig();
            if (isset($config['dimensions'])) {
                $this->dimensions = $config['dimensions'];
            }
        }

        if (!$this->dimensions) {
            throw new ImageException('Dimension is missing', 671);
        }
    }
}
