<?php
declare(strict_types=1);

namespace Api\Models;

use Api\Exceptions\ValidationException;
use Phalcon\Db\ResultInterface;
use Phalcon\Di;

class OfferRequests extends AbstractOfferRequests
{
    /**
     * @param $status
     * @return bool
     * @throws ValidationException
     */
    public function validateStatus($status)
    {
        $db = Di::getDefault()->getShared('db');

        /** @var ResultInterface $result */
        $result = $db->query(/** @lang SQL */ "
            SELECT SUBSTRING(COLUMN_TYPE,6) AS types
            FROM information_schema.COLUMNS
            WHERE TABLE_NAME='offer_requests'
            AND COLUMN_NAME='status'
        ");

        $enumTypes = $result->fetch();
        preg_match_all('/\'([a-z]*)\'/', $enumTypes['types'], $m);
        $statusEnumTypes = $m[1];

        if (!in_array($status, $statusEnumTypes)) {
            throw new ValidationException("Status is not valid (status: '{$status}', items:" . json_encode($enumTypes['types']) . "). It not included in part searches status's status column's enum type.");
        }

        return true;
    }

    /**
     * @param string $status
     * @return OfferRequests
     * @throws ValidationException
     */
    public function setHistory($status)
    {
        if ($this->validateStatus($status))
        {
            $oldHistoryJSON = $this->getHistory();
            if ($oldHistoryJSON) {
                $oldHistoryArr = json_decode($oldHistoryJSON);
                $oldHistoryArr[] = [
                    'status' => $status,
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $this->history = json_encode($oldHistoryArr);
            } else {
                $this->history = json_encode([[
                    'status' => $status,
                    'updated_at' => date('Y-m-d H:i:s')
                ]]);
            }
        }

        return $this;
    }
}
