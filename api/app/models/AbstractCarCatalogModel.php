<?php

namespace Api\Models;

/**
 * AbstractCarCatalogModel
 * 
 * @package Api\Models
 * @autogenerated by Phalcon Developer Tools
 * @date 2020-06-27, 09:49:10
 */
abstract class AbstractCarCatalogModel extends AbstractModel
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="car_catalog_make_id", type="integer", length=11, nullable=true)
     */
    protected $car_catalog_make_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=100, nullable=true)
     */
    protected $name;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field car_catalog_make_id
     *
     * @param integer $car_catalog_make_id
     * @return $this
     */
    public function setCarCatalogMakeId($car_catalog_make_id)
    {
        $this->car_catalog_make_id = $car_catalog_make_id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field car_catalog_make_id
     *
     * @return integer
     */
    public function getCarCatalogMakeId()
    {
        return $this->car_catalog_make_id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("c1bontosokDB");
        $this->setSource("car_catalog_model");
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AbstractCarCatalogModel[]|AbstractCarCatalogModel|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AbstractCarCatalogModel|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
