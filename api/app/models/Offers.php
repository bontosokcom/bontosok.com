<?php
declare(strict_types=1);

namespace Api\Models;

use Api\Exceptions\HTTP401UnauthorizedException;
use Phalcon\Db\Enum;
use Phalcon\Db\ResultInterface;
use Phalcon\Di;

class Offers extends AbstractOffers
{
    /**
     * @return mixed
     * @throws HTTP401UnauthorizedException
     */
    public static function findReceivedOffers()
    {
        $usersId = Di::getDefault()->getShared('session')->get('id');
        if (empty($usersId)) {
            throw new HTTP401UnauthorizedException('User is unauthorized');
        }

        $query = "SELECT o.*, ps.my_cars_id, os.status AS offers_status"
            . " FROM offers AS o"
            . " JOIN part_searches AS ps ON (ps.id = o.part_searches_id)"
            . " JOIN offer_requests_status AS ors ON (ors.id = o.offer_requests_status_id)"
            . " LEFT JOIN offers_status AS os ON (os.offers_id = o.id)"
            . " WHERE o.status = 'active' AND o.deleted = 0 AND o.users_id != {$usersId}"
            . " AND ps.users_id = {$usersId} AND ps.status = 'active' AND ps.deleted = 0"
            . " AND ors.status = 'progress' AND ors.users_id != {$usersId}"
            . " ORDER BY o.created_at DESC";

        /** @var ResultInterface $result */
        $result = Di::getDefault()->getShared('db')->query($query);
        $result->setFetchMode(Enum::FETCH_ASSOC);
        return $result->fetchAll();
    }
}
