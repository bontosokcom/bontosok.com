<?php

namespace Api\Models;

use Api\Exceptions\HTTP404NotFoundException;
use Phalcon\Db\Enum;
use Phalcon\Di;
use Phalcon\Mvc\Model\ResultInterface;

class Users extends AbstractUsers
{
    /**
     * @param $id
     * @return AbstractUsers|ResultInterface
     * @throws HTTP404NotFoundException
     */
    public static function findFirstActiveById(int $id)
    {
        $user = self::findFirst([
            "
                id = :id: 
                AND status = 'active'
                AND deleted = 0 
            ",
            "bind" => [
                "id" => $id
            ]
        ]);

        if (!$user) {
            throw new HTTP404NotFoundException('User Not found');
        }

        return $user;
    }

    /**
     * @param $email
     * @return AbstractUsers|ResultInterface
     * @throws HTTP404NotFoundException
     */
    public static function findFirstActiveByEmail(string $email)
    {
        $user = self::findFirst([
            "
                email = :email: 
                AND status = 'active'
                AND deleted = 0 
            ",
            "bind" => [
                "email" => $email
            ]
        ]);

        if (!$user) {
            throw new HTTP404NotFoundException('User Not found');
        }

        return $user;
    }

    /**
     * @param $resetPasswordToken
     * @return AbstractUsers|ResultInterface
     * @throws HTTP404NotFoundException
     */
    public static function findFirstActiveByResetPasswordToken(string $resetPasswordToken)
    {
        $user = self::findFirst([
            "
                reset_password_token = :reset_password_token: 
                AND status = 'active'
                AND deleted = 0 
            ",
            "bind" => [
                "reset_password_token" => $resetPasswordToken
            ]
        ]);

        if (!$user) {
            throw new HTTP404NotFoundException('User Not found');
        }

        return $user;
    }

    /**
     * @param $activationToken
     * @return AbstractUsers|ResultInterface
     * @throws HTTP404NotFoundException
     */
    public static function findFirstByActivationToken(string $activationToken)
    {
        $user = Users::findFirst([
            'activation_token = :activation_token:',
            'bind' => [
                'activation_token' => $activationToken
            ]
        ]);

        if (!$user) {
            throw new HTTP404NotFoundException('User Not found');
        }

        return $user;
    }

    /**
     * @param $dismantledCarsId
     * @return mixed
     */
    public static function getFirstByDismantledCarsId($dismantledCarsId)
    {
        $query = "SELECT u.*"
            . " FROM users AS u"
            . " JOIN dismantled_cars AS dc ON (dc.users_id = u.id)"
            . " WHERE dc.id = {$dismantledCarsId}";

        /** @var \Phalcon\Db\ResultInterface $result */
        $result = Di::getDefault()->getShared('db')->query($query);
        $result->setFetchMode(Enum::FETCH_ASSOC);
        return $result->fetch();
    }
}
