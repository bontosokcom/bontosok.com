<?php
declare(strict_types=1);

namespace Api\Models;

class PartSearches extends AbstractPartSearches
{
    public function getImagesObject()
    {
        $imagesJson = $this->getImagesJson();
        if (!empty($imagesJson)) {
            return json_decode($imagesJson);
        }

        return [];
    }
}
