<?php

use \Phalcon\Loader;

$loader = new Loader();

$loader->registerFiles([ BASE_PATH . DS . 'vendor/autoload.php' ]);

/** @var $config */
$loader->registerNamespaces([
    'Api\Exceptions'       => $config->application->exceptionsDir,
    'Api\Helpers'          => $config->application->helpersDir,
    'Api\Library'          => $config->application->libraryDir,
    'Api\Library\Image'    => $config->application->libraryDir . '/image',
    'Api\Library\Mailer'   => $config->application->libraryDir . '/mailer',
    'Api\Listeners'        => $config->application->listenersDir,
    'Api\Controllers'      => $config->application->controllersDir,
    'Api\Models'           => $config->application->modelsDir,
    'Api\Transactions'     => $config->application->transactionsDir,
    'Api\Plugins'          => $config->application->pluginsDir
]);

$loader->register();
