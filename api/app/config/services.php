<?php
declare(strict_types=1);

use Api\Listeners\PreFlightListener;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Stream as SessionAdapter;
use Phalcon\Session\Manager as SessionManager;
use Phalcon\Session\Bag as SessionBag;
use Phalcon\Url as UrlResolver;
use Phalcon\Http\Response;
use Phalcon\Http\Response\Cookies;
use Api\Library\Mailer\Manager;

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Registering a Http\Response
 */
$di->set('response', function () {
    $response = new Response();
    $response->setContentType('application/json, charset=UTF-8');
    $response->setRawHeader('HTTP/1.1 200 OK');

    return $response;
});

/**
 * Preflight
 */
$di->set('preflight', function() {
    return new PreFlightListener();
}, true);

/**
 * Register router
 */
$di->setShared('router', function () {
    return new Router(false);
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'path' => $config->application->cacheDir,
                'separator' => '_'
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $params = [
        'host'     => $config->database->host,
        'port'     => $config->database->port,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset,
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        ]
    ];

    $connection = new Mysql($params);

    $eventsManager = new EventsManager();
    $eventsManager->attach('db',
        function (Event $event, Mysql $connection) {
            switch ($event->getType()) {
                case 'beforeQuery':
                    $adapter = new Stream(BASE_PATH . '/logs/sql-log-' . date("Y-m-d") . '.log');
                    $logger  = new Logger('messages', ['main' => $adapter]);
                    $logger->info($connection->getSqlStatement()
                        . ' | '
                        . (ENVIRONMENT == 'development'
                            ? json_encode($connection->getSqlVariables())
                            : '')
                    );
                    break;
            }
        }
    );

    $connection->setEventsManager($eventsManager);

    return $connection;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionManager();

    $path = getenv('SESSIONS_PATH') ? BASE_PATH . getenv('SESSIONS_PATH') : sys_get_temp_dir();
    if (!is_dir($path)) {
        mkdir($path);
    }

    $files = new SessionAdapter([
        'savePath' => $path,
    ]);
    $session->setAdapter($files);
    $session->start();

    return $session;
});

$di->setShared(
    'sessionBag',
    function () {
        return new SessionBag("sessionBag");
    }
);

$di->set(
    'cookies',
    function () {
        $cookies = new Cookies();
        $cookies->useEncryption(false);
        return $cookies;
    }
);

$di->setShared(
    'logger',
    function() {
        $adapter = new Stream(BASE_PATH . '/logs/debug-log-' . date("Y-m-d") . '.log');
        return new Logger('messages', ['debug' => $adapter]);
    }
);

$di->setShared(
    'mailer',
    function () {
        return new Manager();
    }
);
