<?php

use Phalcon\Events\Event;
use Phalcon\Events\Manager;
use \Phalcon\Mvc\Dispatcher;
use Api\Plugins\SecurityPlugin;

$di->set(
    'dispatcher',
    function() use ($di) {

        $eventsManager = new Manager();

        $eventsManager->attach(
            'dispatch:beforeExecuteRoute',
            $di->get('preflight')
        );

        $eventsManager->attach(
            'dispatch:beforeExecuteRoute',
            new SecurityPlugin()
        );

        $eventsManager->attach(
            'dispatch:afterExecuteRoute',
            function (Event $event, Dispatcher $dispatcher) use ($di) {
                /** @var Dispatcher $dispatcher */
                $return = $dispatcher->getReturnedValue();
                $di->getShared('response')->setJsonContent($return);
                $di->getShared('response')->send();
            }
        );

        $dispatcher = new Dispatcher();
        $dispatcher->setEventsManager($eventsManager);
        return $dispatcher;
    },
    true
);
