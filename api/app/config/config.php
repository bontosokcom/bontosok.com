<?php

use Phalcon\Config;

define('DS',                    DIRECTORY_SEPARATOR);

define('BASE_PATH',             realpath(dirname(__FILE__) . '/../..'));
define('APP_PATH',              BASE_PATH . DS . 'app');
define('PUBLIC_PATH',           BASE_PATH . DS . 'public');
define('STATIC_PATH',           PUBLIC_PATH . DS . 'static');
define('STATIC_TEMP_PATH',      STATIC_PATH . DS . 'temp');
define('STATIC_UPLOADS_PATH',   STATIC_PATH . DS . 'uploads');

define('BASE_URL',              "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}");
define('STATIC_URL',            BASE_URL . DS . "static");
define('TEMP_URL',              STATIC_URL . DS . "temp");
define('UPLOADS_URL',           STATIC_URL . DS . "uploads");

include BASE_PATH . DS . 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(BASE_PATH, '.env');
$dotenv->load();

return new Config([
    'database' => [
        'adapter'           => getenv('DB_ADAPTER'),
        'host'              => getenv('DB_HOST'),
        'port'              => getenv('DB_PORT'),
        'username'          => getenv('DB_USERNAME'),
        'password'          => getenv('DB_PASSWORD'),
        'dbname'            => getenv('DB_NAME'),
        'charset'           => getenv('DB_CHARSET')
    ],
    'application' => [
        'appDir'            => APP_PATH . '/',
        'listenersDir'      => APP_PATH . '/listeners/',
        'exceptionsDir'     => APP_PATH . '/exceptions/',
        'controllersDir'    => APP_PATH . '/controllers/',
        'modelsDir'         => APP_PATH . '/models/',
        'transactionsDir'   => APP_PATH . '/transactions/',
        'migrationsDir'     => APP_PATH . '/migrations/',
        'viewsDir'          => APP_PATH . '/views/',
        'pluginsDir'        => APP_PATH . '/plugins/',
        'helpersDir'        => APP_PATH . '/helpers/',
        'libraryDir'        => APP_PATH . '/library/',
        'cacheDir'          => BASE_PATH . '/cache/',
        'baseUri'           => '/',
    ],
    'dimensions' => [
        'thumbnail' => '250x250',
        'default' => '600x450'
    ]
]);
