<?php

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group as RouterGroup;

/** @var Router $router */
/** @var RouterGroup $auth */

$router = $di->getRouter();

$api = new RouterGroup();

$api->setPrefix('/api');

$api->add('/login',                                      'Api\Controllers\Auth::login', ['POST']);
$api->add('/logout',                                     'Api\Controllers\Auth::logout', ['GET']);

$api->add('/image-upload',                               'Api\Controllers\ImageUpload::uploadImage', ['POST']);

$api->add('/registration',                               'Api\Controllers\Users::registration', ['POST']);
$api->add('/confirm-registration',                       'Api\Controllers\Users::confirmRegistration', ['GET']);
$api->add('/forgot-password',                            'Api\Controllers\Users::forgotPassword', ['POST']);
$api->add('/reset-password',                             'Api\Controllers\Users::resetPassword', ['POST']);
$api->add('/get-logged-in-user',                         'Api\Controllers\Users::getLoggedInUser', ['GET']);
$api->add('/get-email-by-reset-password-token',          'Api\Controllers\Users::getEmailByResetPasswordToken', ['GET']);
$api->add('/update-personal',                            'Api\Controllers\Users::updatePersonal', ['POST']);
$api->add('/change-password',                            'Api\Controllers\Users::changePassword', ['POST']);
$api->add('/change-email',                               'Api\Controllers\Users::changeEmail', ['POST']);
$api->add('/change-type',                                'Api\Controllers\Users::changeType', ['POST']);
$api->add('/delete-account',                             'Api\Controllers\Users::deleteAccount', ['POST']);

$api->add('/car-catalog',                                'Api\Controllers\CarCatalog::catalog', ['GET']);

$api->add('/add-dismantled-car',                         'Api\Controllers\DismantledCars::addDismantledCar', ['POST']);
$api->add('/get-dismantled-cars',                        'Api\Controllers\DismantledCars::getDismantledCars', ['GET']);
$api->add('/set-dismantled-cars-status',                 'Api\Controllers\DismantledCars::setDismantledCarsStatus', ['PUT']);
$api->add('/delete-dismantled-car',                      'Api\Controllers\DismantledCars::deleteDismantledCar', ['DELETE']);

$api->add('/add-my-car',                                 'Api\Controllers\MyCars::addMyCar', ['POST']);
$api->add('/get-my-cars',                                'Api\Controllers\MyCars::getMyCars', ['GET']);
$api->add('/delete-my-car',                              'Api\Controllers\MyCars::deleteMyCar', ['DELETE']);



$api->add('/start-part-search',                          'Api\Controllers\PartSearches::startPartSearch', ['POST']);
$api->add('/get-part-searches',                          'Api\Controllers\PartSearches::getPartSearches', ['GET']);
$api->add('/set-part-search-status',                     'Api\Controllers\PartSearches::setPartSearchStatus', ['PUT']);
$api->add('/delete-part-search',                         'Api\Controllers\PartSearches::deletePartSearch', ['DELETE']);

$api->add('/get-offer-requests',                         'Api\Controllers\OfferRequests::getOfferRequests', ['GET']);
$api->add('/set-offer-requests-status',                  'Api\Controllers\OfferRequests::setOfferRequestsStatus', ['POST']);

$api->add('/send-offer',                                 'Api\Controllers\Offers::sendOffer', ['POST']);
//$api->add('/get-phone-by-offer',                         'Api\Controllers\Offers::getPhoneByOffer', ['GET']);

$api->add('/get-received-offers',                        'Api\Controllers\ReceivedOffers::getReceivedOffers', ['GET']);
$api->add('/set-received-offers-status',                 'Api\Controllers\ReceivedOffers::setReceivedOffersStatus', ['POST']);

$router->mount($api);

$router->add('/',                                        'Api\Controllers\Error::show404');

$router->notFound('Api\Controllers\Error::show404');

$router->handle($_SERVER['REQUEST_URI']);
