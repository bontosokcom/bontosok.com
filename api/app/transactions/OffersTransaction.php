<?php
declare(strict_types=1);

namespace Api\Transactions;

use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Models\Offers;

class OffersTransaction extends Transactions
{
    /**
     * @param $offerRequestsId
     * @return bool
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     */
    public static function inactivateOffer($offerRequestsId)
    {
        $offer = Offers::findFirst([
            "
                offer_requests_id=:offer_requests_id:
            ",
            'bind' => [
                'offer_requests_id' => $offerRequestsId
            ]
        ]);
        if (!$offer) {
            throw new HTTP404NotFoundException('Offer not found');
        }

        $offer->setStatus('inactive');

        if (parent::update($offer)) {
            return true;
        }
    }
}
