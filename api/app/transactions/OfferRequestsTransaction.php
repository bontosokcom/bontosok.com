<?php
declare(strict_types=1);

namespace Api\Transactions;

use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Exceptions\ValidationException;
use Api\Models\DismantledCars;
use Api\Models\OfferRequests;
use Api\Models\PartSearches;

class OfferRequestsTransaction extends Transactions
{
    /**
     * @param $partSearchesId
     * @param $status
     * @return bool
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     * @throws HTTP409ConflictException
     * @throws HTTPException
     * @throws ValidationException
     */
    public static function insertOrUpdateOfferRequestsByPartSearchesId($partSearchesId, $status)
    {
        $partSearch = PartSearches::findFirst([
            "
                id=:id: 
                AND deleted=0 
                AND status='active'
            ",
            'bind' => [
                'id' => $partSearchesId
            ]
        ]);
        if (!$partSearch) {
            throw new HTTP404NotFoundException('Part search not found');
        }

        $dismantledCar = DismantledCars::findFirst([
            "
                users_id=:users_id:
                AND car_code=:car_code:
            ",
            'bind' => [
                'users_id' => parent::getUsersId(),
                'car_code' => $partSearch->getCarCode()
            ]
        ]);
        if (!$dismantledCar) {
            throw new HTTP404NotFoundException('Dismantled car not found');
        }

        $offerRequest = OfferRequests::findFirst(["
            users_id=:users_id:
            AND dismantled_cars_id=:dismantled_cars_id:
            AND part_searches_id=:part_searches_id:         
        ", 'bind' => [
            'users_id' => parent::getUsersId(),
            'dismantled_cars_id' => $dismantledCar->getId(),
            'part_searches_id' => $partSearchesId
        ]]);

        if ($offerRequest) {
            $offerRequest->setStatus($status);
            $offerRequest->setHistory($status);

            if (OfferRequestsTransaction::update($offerRequest)) {

                if (in_array($status, ['revoked', 'succeed', 'closed'])) {
                    OffersTransaction::inactivateOffer($offerRequest->getId());
                }

                return $offerRequest->getId();
            }
        } else {
            $offerRequest = new OfferRequests();

            $offerRequest->setUsersId(parent::getUsersId());
            $offerRequest->setDismantledCarsId($dismantledCar->getId());
            $offerRequest->setPartSearchesId($partSearchesId);
            $offerRequest->setCarCode($partSearch->getCarCode());
            $offerRequest->setStatus($status);
            $offerRequest->setHistory($status);

            if (OfferRequestsTransaction::create($offerRequest)) {
                return $offerRequest->getId();
            }
        }
    }
}
