<?php
declare(strict_types=1);

namespace Api\Transactions;

use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Models\AbstractDismantledCars;
use Api\Models\AbstractModel;
use Api\Models\AbstractMyCars;
use Api\Models\CarCatalogMake;
use Api\Models\CarCatalogModel;
use Api\Models\CarCatalogPlatform;
use Api\Models\CarCatalogYear;

class CommonCarsTransaction extends Transactions
{
    /**
     * @param AbstractModel | AbstractMyCars | AbstractDismantledCars $payload
     * @return mixed
     * @throws HTTP409ConflictException
     * @throws HTTPException
     */
    public static function create($payload)
    {
        $carInfo = self::getCarInfo($payload->getCarCode());

        $payload->setMake($carInfo['make']);
        $payload->setModel($carInfo['model']);
        $payload->setPlatform($carInfo['platform']);
        $payload->setYear($carInfo['year']);

        return parent::create($payload);
    }

    /**
     * @param string $carCode
     * @return array
     * @throws HTTPException
     */
    private static function getCarInfo($carCode)
    {
        $carInfoIds = self::parseCarCode($carCode);
        $carInfo = [
            'make' => '',
            'model' => '',
            'platform' => '',
            'year' => ''
        ];
        if ($make = CarCatalogMake::findFirst(["id={$carInfoIds['MA']}", 'column' => 'name'])) {
            $carInfo['make'] = $make->getName();
        }
        if ($model = CarCatalogModel::findFirst(["id={$carInfoIds['MO']}", 'column' => 'name'])) {
            $carInfo['model'] = $model->getName();
        }
        if ($platform = CarCatalogPlatform::findFirst(["id={$carInfoIds['PL']}", 'column' => 'name'])) {
            $carInfo['platform'] = $platform->getName();
        }
        if ($year = CarCatalogYear::findFirst(["id={$carInfoIds['YE']}", 'column' => 'name'])) {
            $carInfo['year'] = $year->getName();
        }

        if (empty($carInfo['make']) || empty($carInfo['model']) || empty($carInfo['platform']) || empty($carInfo['year'])) {
            throw new HttpException('Invalid car code', 701);
        }

        return $carInfo;
    }

    /**
     * @param $carCode
     * @return array
     * @throws HTTPException
     */
    private static function parseCarCode($carCode)
    {
        preg_match('/^MA([0-9]*)_MO([0-9]*)_PL([0-9]*)_YE([0-9]*)$/', $carCode, $carCodeArr);

        if (count($carCodeArr) != 5) {
            throw new HttpException('Invalid car code', 701);
        }

        return [
            'MA' => $carCodeArr[1],
            'MO' => $carCodeArr[2],
            'PL' => $carCodeArr[3],
            'YE' => $carCodeArr[4]
        ];
    }
}
