<?php
declare(strict_types=1);

namespace Api\Transactions;

use Api\Exceptions\HTTP409ConflictException;
use Api\Models\DismantledCars;

class DismantledCarsTransaction extends CommonCarsTransaction
{
    public static function create($payload)
    {
        $dismantledCar = DismantledCars::findFirst(["
            users_id=:users_id:
            AND deleted=0
            AND car_code=:car_code:
        ", 'bind' => [
            'users_id' => $payload->getUsersId(),
            'car_code' => $payload->getCarCode()
        ]]);
        if ($dismantledCar) {
            throw new HTTP409ConflictException('It is already exists');
        }

        return parent::create($payload);
    }
}
