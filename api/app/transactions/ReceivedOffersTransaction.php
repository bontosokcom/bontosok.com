<?php
declare(strict_types=1);

namespace Api\Transactions;

use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Exceptions\ValidationException;
use Api\Models\Offers;
use Api\Models\ReceivedOffers;

class ReceivedOffersTransaction extends Transactions
{
    /**
     * @param $offersId
     * @param $partSearchesId
     * @param $status
     * @return bool
     * @throws HTTP409ConflictException
     * @throws HTTPException
     * @throws ValidationException
     * @throws HTTP401UnauthorizedException
     * @throws HTTP404NotFoundException
     */
    public static function insertOrUpdateReceivedOffersStatus($offersId, $partSearchesId, $status)
    {
        $receivedOffers = ReceivedOffers::findFirst(["
            users_id=:users_id:
            AND offers_id=:offers_id:
            AND part_searches_id=:part_searches_id:         
        ", 'bind' => [
            'users_id' => parent::getUsersId(),
            'offers_id' => $offersId,
            'part_searches_id' => $partSearchesId
        ]]);

        if ($receivedOffers) {
            $receivedOffers->setStatus($status);
            $receivedOffers->setHistory($status);

            if (ReceivedOffersTransaction::update($receivedOffers)) {
                return $receivedOffers->getId();
            }
        } else {
            $offer = Offers::findFirst([
                "
                    users_id!=:users_id:
                    AND id=:id:
                ",
                'bind' => [
                    'users_id' => parent::getUsersId(),
                    'id' => $offersId
                ]
            ]);

            if (!$offer) {
                throw new HTTP404NotFoundException('Offer not found');
            }

            $receivedOffers = new ReceivedOffers();

            $receivedOffers->setUsersId(parent::getUsersId());
            $receivedOffers->setDismantledCarsId($offer->getDismantledCarsId());
            $receivedOffers->setPartSearchesId($partSearchesId);
            $receivedOffers->setOfferRequestsId($offer->getOfferRequestsId());
            $receivedOffers->setOffersId($offersId);
            $receivedOffers->setCarCode($offer->getCarCode());
            $receivedOffers->setStatus($status);
            $receivedOffers->setHistory($status);

            if (ReceivedOffersTransaction::create($receivedOffers)) {
                return $receivedOffers->getId();
            }
        }
    }

}
