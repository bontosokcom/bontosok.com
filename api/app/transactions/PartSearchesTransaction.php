<?php
declare(strict_types=1);

namespace Api\Transactions;

use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Models\PartSearches;

class PartSearchesTransaction extends Transactions
{
    /**
     * @param PartSearches $payload
     * @return mixed
     * @throws HTTP409ConflictException
     * @throws HTTPException
     */
    public static function create($payload)
    {
        $myCar = PartSearches::findFirst(["
            users_id=:users_id:
            AND deleted=0
            AND car_code=:car_code:
            AND title=:title:
            AND description=:description:
        ", 'bind' => [
            'users_id' => $payload->getUsersId(),
            'car_code' => $payload->getCarCode(),
            'title' => $payload->getTitle(),
            'description' => $payload->getDescription()
        ]]);
        if ($myCar) {
            throw new HTTP409ConflictException('It is already exists');
        }

        return parent::create($payload);
    }
}
