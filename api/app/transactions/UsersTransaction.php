<?php
declare(strict_types=1);

namespace Api\Transactions;

class UsersTransaction extends Transactions
{
    public static function delete($payload)
    {
        DismantledCarsTransaction::deleteAll();
        MyCarsTransaction::deleteAll();

        return parent::delete($payload);
    }
}
