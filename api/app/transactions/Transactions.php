<?php
declare(strict_types=1);

namespace Api\Transactions;

use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP409ConflictException;
use Api\Exceptions\HTTPException;
use Api\Models\AbstractModel;
use PDOException;
use Phalcon\Di;

class Transactions
{
    /**
     * @param $payload AbstractModel
     * @return mixed
     * @throws HTTP409ConflictException
     * @throws HTTPException
     */
    public static function create($payload)
    {
        $relatedModelName = self::getRelatedModelName();

        $payload->setCreatedAt(date('Y-m-d H:i:s'));

        try {
            if ($payload->create()) {
                $newItem = $relatedModelName::findFirst([
                    'id = :id:',
                    'columns' => 'id',
                    'bind' => [
                        'id' => $payload->getId()
                    ]
                ]);

                if ($newItem) {
                    return $newItem->id;
                } else {
                    throw new HTTP409ConflictException('While check creating');
                }
            } else {
                $message = empty($payload->getMessages())
                    ? 'While creating'
                    : 'VALIDATION ERROR: ' . json_encode($payload->getMessages());

                throw new HTTP409ConflictException($message);
            }
        } catch (PDOException $e) {
            throw new HTTP409ConflictException($e->getMessage());
        }
    }

    /**
     * @param $payload AbstractModel
     * @return bool
     * @throws HTTP409ConflictException
     */
    public static function update($payload)
    {
        $payload->setUpdatedAt(date('Y-m-d H:i:s'));

        try {
            if ($payload->update()) {
                return true;
            } else {
                $message = empty($payload->getMessages())
                    ? 'While updating'
                    : 'VALIDATION ERROR: ' . json_encode($payload->getMessages());

                throw new HTTP409ConflictException($message);
            }
        } catch (PDOException $e) {
            throw new HTTP409ConflictException($e->getMessage());
        }
    }

    /**
     * @param $payload AbstractModel
     * @return bool
     * @throws HTTP409ConflictException
     */
    public static function delete($payload)
    {
        $payload->setStatus('inactive');
        $payload->setDeleted(1);
        $payload->setDeletedAt(date('Y-m-d H:i:s'));

        return self::update($payload);
    }

    public static function deleteAll()
    {
        $relatedModelName = self::getRelatedModelName();
        $results = $relatedModelName::find([
            "users_id=:users_id:",
            'bind' => [
                'users_id' => self::getUsersId()
            ]
        ]);
        foreach ($results as $result) {
            self::delete($result);
        }

        return true;
    }

    /**
     * @return string|string[]
     * @throws HTTPException
     */
    private static function getRelatedModelName()
    {
        $calledClassName = get_called_class();
        $calledClassNameArr = explode('\\', $calledClassName);
        if (count($calledClassNameArr) && isset($calledClassNameArr[2])) {
            return 'Api\Models\\' . str_replace('Transaction', '', $calledClassNameArr[2]);
        }
        throw new HttpException('Wrong related model name', 700);
    }

    /**
     * @return int
     * @throws HTTP401UnauthorizedException
     */
    public static function getUsersId()
    {
        $usersId = Di::getDefault()->getShared('session')->get('id');

        if (empty($usersId)) {
            throw new HTTP401UnauthorizedException('User is unauthorized: ' . $usersId);
        }
        return (int) $usersId;
    }
}
