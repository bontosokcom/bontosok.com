<?php
declare(strict_types=1);

namespace Api\Exceptions;

class HTTP404NotFoundException extends AbstractException
{
    protected $message = 'Not Found';
    protected $code = 404;
}
