<?php
declare(strict_types=1);

namespace Api\Exceptions;

class HelperException extends AbstractException
{
    protected $message = 'Helper exception';
    protected $code = 710;
}
