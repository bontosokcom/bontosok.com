<?php
declare(strict_types=1);

namespace Api\Exceptions;

class ValidationException extends AbstractException
{
    //
}
