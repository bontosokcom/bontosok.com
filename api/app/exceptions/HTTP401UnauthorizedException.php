<?php
declare(strict_types=1);

namespace Api\Exceptions;

class HTTP401UnauthorizedException extends AbstractException
{
    protected $message = 'Unauthorized';
    protected $code = 401;
}
