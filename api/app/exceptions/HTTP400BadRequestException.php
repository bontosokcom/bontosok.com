<?php
declare(strict_types=1);

namespace Api\Exceptions;

class HTTP400BadRequestException extends AbstractException
{
    protected $message = 'Bad request';
    protected $code = 400;
}
