<?php
declare(strict_types=1);

namespace Api\Exceptions;

use Phalcon\Exception;

class AbstractException extends Exception
{
    //
}
