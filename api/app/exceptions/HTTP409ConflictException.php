<?php
declare(strict_types=1);

namespace Api\Exceptions;

class HTTP409ConflictException extends AbstractException
{
    protected $message = 'Conflict';
    protected $code = 409;
}
