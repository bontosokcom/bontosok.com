{# templates/email/confirm-registration.volt #}
{% extends 'templates/email/layout.volt' %}

{% block content %}
<p>Az email-címedhez tartozó felhasználói fiók jelszavához módosítási kérelem érkezett.<br>
    Amennyieben nem te indítottad nyugodtan hagyd figyelmen kívül ezt a levelet. <br>
    Amennyiben te indítottad, az alábbi link segítségével tudsz új jelszót megadni.
</p>
<p><a href="{{ passwordResetLink }}">{{ passwordResetLinkAlias }}</a></p>
{% endblock %}
