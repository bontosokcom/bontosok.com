{# templates/email/layout.volt #}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Demystifying Email Design</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

{% include 'templates/email/header.volt' %}

<tr>
    <td><h1>Kedves {% if recipient is defined %}{{ recipient }}{% else %}Felhasználó{% endif %}!</h1></td>
</tr>
<tr>
    <td>
        {% block content %}{% endblock %}
    </td>
</tr>
<tr>
    <td>
        <p>Üdvözlettel a Bontosok.com csapata!</p>
    </td>
</tr>

{% include 'templates/email/footer.volt' %}

</table>
</body>
</html>
