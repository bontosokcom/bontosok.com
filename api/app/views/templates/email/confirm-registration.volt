{# templates/email/confirm-registration.volt #}
{% extends 'templates/email/layout.volt' %}

{% block content %}
<p>Ezzel az email-címmel regisztráltak oldalunkon. <br>
    Amennyiben nem te regisztráltál, úgy kérjük, hogy tekinsd tárgytalannak ezt a levelet. <br>
    Amennyiben te regisztráltál az alábbi linken tudod megerősíteni regisztrációadat.</p>
<p><a href="{{ activationLink }}">{{ activationLinkAlias }}</a></p>
{% endblock %}
