<?php
declare(strict_types=1);

namespace Api\Plugins;

use Firebase\JWT\JWT;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Component;
use Phalcon\Acl\Role;
use Phalcon\Acl\Enum;
use Phalcon\Di\Injectable;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Api\Exceptions\HTTP401UnauthorizedException;
use Api\Exceptions\HTTP404NotFoundException;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin extends Injectable
{
    /**
     * This action is executed before execute any action in the application
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     * @throws HTTP404NotFoundException
     * @throws HTTP401UnauthorizedException
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $authorization = $this->request->getHeader('Authorization');
        $bearerToken = preg_replace("/Bearer /", "", $authorization);
        $sessionToken = $this->session->get('token');

        if ($bearerToken === $sessionToken) {
            $token = JWT::decode($sessionToken, getenv('JWT_KEY'), ['HS256']);
            $role = ucfirst($token->role);
        } else {
            $role = 'Unauthorized';
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $acl = $this->getAcl();

        if (!$acl->isComponent($controller)) {
            throw new HTTP404NotFoundException('Component Not found');
        }

        $allowed = $acl->isAllowed($role, $controller, $action);
        if (!$allowed) {
            $this->session->destroy();
            $this->cookies->set('token', null);

            throw new HTTP401UnauthorizedException('Unauthorized or Permission denied. Role: ' . $role);
        }

        return true;
    }

    /**
     * Returns an existing or new access control list
     *
     * @returns AclList
     */
    protected function getAcl(): AclList
    {
        if (isset($this->persistent->acl)) {
            return $this->persistent->acl;
        }

        $acl = new AclList();
        $acl->setDefaultAction(Enum::DENY);

        // Register roles
        $roles = [
            'authorized'  => new Role(
                'Authorized',
                'Users was authorized'
            ),
            'unauthorized'  => new Role(
                'Unauthorized',
                'Unauthorized users'
            ),
        ];

        foreach ($roles as $role) {
            $acl->addRole($role);
        }

        //Private area resources
        $privateResources = [
            'auth'          => [
                'logout'
            ],
            'image_upload'  => [
                'uploadImage'
            ],
            'users'         => [
                'getLoggedInUser',
                'updatePersonal',
                'changePassword',
                'changeEmail',
                'changeType',
                'deleteAccount'
            ],
            'car_catalog' => [
                'catalog'
            ],
            'dismantled_cars' => [
                'addDismantledCar',
                'getDismantledCars',
                'setDismantledCarsStatus',
                'deleteDismantledCar'
            ],
            'my_cars' => [
                'addMyCar',
                'getMyCars',
                'deleteMyCar'
            ],
            'part_searches' => [
                'startPartSearch',
                'getPartSearches',
                'setPartSearchStatus',
                'deletePartSearch'
            ],
            'offer_requests' => [
                'getOfferRequests',
                'setOfferRequestsStatus'
            ],
            'offers' => [
                'sendOffer',
//                'getPhoneByOffer'
            ],
            'received_offers' => [
                'getReceivedOffers',
                'setReceivedOffersStatus'
            ]
        ];
        foreach ($privateResources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }

        //Public area resources
        $publicResources = [
            'auth'          => ['login'],
            'users'         => [
                'registration',
                'confirmRegistration',
                'forgotPassword',
                'resetPassword',
                'getEmailByResetPasswordToken'
            ],
            'errors'        => ['show401', 'show404'],
        ];
        foreach ($publicResources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }

        //Grant access to public areas to unauthorized
        foreach ($roles as $role) {
            foreach ($publicResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow($role->getName(), $resource, $action);
                }
            }
        }

        //Grant access to private area to role Admin and Member
        foreach ($privateResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow('Authorized', $resource, $action);
            }
        }

        //The acl is stored in session, APC would be useful here too
        $this->persistent->acl = $acl;

        return $acl;
    }
}
