<?php
declare(strict_types=1);

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

try {
    /**
     * The FactoryDefault Dependency Injector
     */
    $di = new FactoryDefault();

    /**
     * Shared configuration service and
     * Get config service for use in inline setup below
     */
    $di->setShared('config', function () {
        return include __DIR__ . "/config/config.php";
    });
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    /**
     * Include Events Manager
     */
    include APP_PATH . '/config/events-manager.php';

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Handle routes
     */
    include APP_PATH . '/config/router.php';

    /**
     * Handle the request
     */
    echo (new Application($di))->handle($_SERVER['REQUEST_URI'])->getContent();

} catch (Exception $e) {
    header("Content-Type: application/json");
    http_response_code($e->getCode());
    echo json_encode([
        'code' => $e->getCode(),
        'message' => $e->getMessage()
    ]);
}
