-- MySQL dump 10.13  Distrib 5.7.23, for osx10.9 (x86_64)
--
-- Host: localhost    Database: c1bontosokDB
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `car_catalog`
--

DROP TABLE IF EXISTS `car_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_catalog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `make` varchar(50) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `car_catalog_make_model_platform_year_uindex` (`make`,`model`,`platform`,`year`)
) ENGINE=InnoDB AUTO_INCREMENT=806 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `car_catalog_make`
--

DROP TABLE IF EXISTS `car_catalog_make`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_catalog_make` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ccma_unique` (`name`),
  KEY `ccma_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `car_catalog_model`
--

DROP TABLE IF EXISTS `car_catalog_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_catalog_model` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_catalog_make_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ccmo_unique` (`car_catalog_make_id`,`name`),
  KEY `ccmo_make_id_index` (`car_catalog_make_id`),
  KEY `ccmo_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=756 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `car_catalog_platform`
--

DROP TABLE IF EXISTS `car_catalog_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_catalog_platform` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_catalog_make_id` int(11) DEFAULT NULL,
  `car_catalog_model_id` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ccpl_unique` (`car_catalog_make_id`,`car_catalog_model_id`,`name`),
  KEY `ccpl_make_id_index` (`car_catalog_make_id`),
  KEY `ccpl_model_id_index` (`car_catalog_model_id`),
  KEY `ccpl_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=756 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `car_catalog_year`
--

DROP TABLE IF EXISTS `car_catalog_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_catalog_year` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_catalog_make_id` int(11) DEFAULT NULL,
  `car_catalog_model_id` int(11) DEFAULT NULL,
  `car_catalog_platform_id` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ccye_unique` (`car_catalog_make_id`,`car_catalog_model_id`,`car_catalog_platform_id`,`name`),
  KEY `ccye_make_id_index` (`car_catalog_make_id`),
  KEY `ccye_model_id_index` (`car_catalog_model_id`),
  KEY `ccye_palatform_id_index` (`car_catalog_platform_id`),
  KEY `ccye_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=756 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dismantled_cars`
--

DROP TABLE IF EXISTS `dismantled_cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dismantled_cars` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) unsigned NOT NULL,
  `status` enum('active','inactive') DEFAULT 'inactive',
  `deleted` tinyint(1) DEFAULT '0',
  `car_code` varchar(50) DEFAULT NULL,
  `make` varchar(50) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `platform` varchar(20) DEFAULT NULL,
  `year` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dismantled_cars_users_id_index` (`users_id`),
  KEY `dismantled_cars_car_code_index` (`car_code`),
  CONSTRAINT `dismantled_cars_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_cars`
--

DROP TABLE IF EXISTS `my_cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_cars` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) unsigned NOT NULL,
  `status` enum('active','inactive') DEFAULT 'inactive',
  `deleted` tinyint(1) DEFAULT '0',
  `car_code` varchar(50) DEFAULT NULL,
  `make` varchar(50) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `platform` varchar(20) DEFAULT NULL,
  `year` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `my_cars_code_index` (`car_code`),
  KEY `my_cars_users_id_index` (`users_id`),
  CONSTRAINT `my_cars_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offer_requests`
--

DROP TABLE IF EXISTS `offer_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offer_requests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) unsigned DEFAULT NULL,
  `dismantled_cars_id` int(11) unsigned DEFAULT NULL,
  `part_searches_id` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `status` enum('archived','arrived','closed','declined','progress','revoked','succeed') DEFAULT 'arrived',
  `history` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offer_requests_dismantled_cars_id_fk` (`dismantled_cars_id`),
  KEY `offer_requests_part_searches_id_fk` (`part_searches_id`),
  KEY `offer_requests_users_dismantled_cars_part_searches_index` (`users_id`,`dismantled_cars_id`,`part_searches_id`),
  CONSTRAINT `offer_requests_dismantled_cars_id_fk` FOREIGN KEY (`dismantled_cars_id`) REFERENCES `dismantled_cars` (`id`),
  CONSTRAINT `offer_requests_part_searches_id_fk` FOREIGN KEY (`part_searches_id`) REFERENCES `part_searches` (`id`),
  CONSTRAINT `offer_requests_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) unsigned DEFAULT NULL,
  `dismantled_cars_id` int(11) unsigned DEFAULT NULL,
  `part_searches_id` int(11) unsigned DEFAULT NULL,
  `offer_requests_id` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `status` enum('active','inactive') DEFAULT 'inactive',
  `details` text,
  `price` int(11) DEFAULT NULL,
  `currency` enum('HUF') DEFAULT NULL,
  `images_json` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offers_dismantled_cars_id_fk` (`dismantled_cars_id`),
  KEY `offers_offer_requests_id_fk` (`offer_requests_id`),
  KEY `offers_part_searches_id_fk` (`part_searches_id`),
  KEY `offers_users_dismantled_cars_part_searches_offer_requests_index` (`users_id`,`dismantled_cars_id`,`part_searches_id`,`offer_requests_id`),
  CONSTRAINT `offers_dismantled_cars_id_fk` FOREIGN KEY (`dismantled_cars_id`) REFERENCES `dismantled_cars` (`id`),
  CONSTRAINT `offers_offer_requests_id_fk` FOREIGN KEY (`offer_requests_id`) REFERENCES `offer_requests` (`id`),
  CONSTRAINT `offers_part_searches_id_fk` FOREIGN KEY (`part_searches_id`) REFERENCES `part_searches` (`id`),
  CONSTRAINT `offers_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `part_searches`
--

DROP TABLE IF EXISTS `part_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `part_searches` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) unsigned DEFAULT NULL,
  `my_cars_id` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `status` enum('active','inactive') DEFAULT 'inactive',
  `car_code` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `images_json` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `part_searches_my_cars_id_fk` (`my_cars_id`),
  KEY `part_searches_users_id_fk` (`users_id`),
  CONSTRAINT `part_searches_my_cars_id_fk` FOREIGN KEY (`my_cars_id`) REFERENCES `my_cars` (`id`),
  CONSTRAINT `part_searches_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `received_offers`
--

DROP TABLE IF EXISTS `received_offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `received_offers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) unsigned DEFAULT NULL,
  `dismantled_cars_id` int(11) unsigned DEFAULT NULL,
  `part_searches_id` int(11) unsigned DEFAULT NULL,
  `offer_requests_id` int(11) unsigned DEFAULT NULL,
  `offers_id` int(11) unsigned DEFAULT NULL,
  `delete` tinyint(1) DEFAULT '0',
  `status` enum('archived','arrived','closed','succeed') DEFAULT NULL,
  `history` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `received_offers_users_dismantled_cars_part_searches_offers_index` (`id`,`users_id`,`dismantled_cars_id`,`part_searches_id`,`offer_requests_id`,`offers_id`),
  KEY `received_offers_users_id_fk` (`users_id`),
  KEY `received_offers_dismantled_cars_id_index` (`dismantled_cars_id`),
  KEY `received_offers_offers_id_index` (`offers_id`),
  KEY `received_offers_part_searches_id_index` (`part_searches_id`),
  KEY `received_offers_offer_requests_id_fk` (`offer_requests_id`),
  CONSTRAINT `received_offers_dismantled_cars_id_fk` FOREIGN KEY (`dismantled_cars_id`) REFERENCES `dismantled_cars` (`id`),
  CONSTRAINT `received_offers_offer_requests_id_fk` FOREIGN KEY (`offer_requests_id`) REFERENCES `offer_requests` (`id`),
  CONSTRAINT `received_offers_offers_id_fk` FOREIGN KEY (`offers_id`) REFERENCES `offers` (`id`),
  CONSTRAINT `received_offers_part_searches_id_fk` FOREIGN KEY (`part_searches_id`) REFERENCES `part_searches` (`id`),
  CONSTRAINT `received_offers_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `deleted` tinyint(1) DEFAULT '0',
  `type` enum('supply','demand') DEFAULT NULL,
  `is_filled` tinyint(1) DEFAULT '0',
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `vat_number` varchar(20) DEFAULT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `registrated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  UNIQUE KEY `users_vat_number_uindex` (`vat_number`),
  KEY `users_activation_token_index` (`activation_token`),
  KEY `users_reset_password_token_index` (`reset_password_token`),
  KEY `users_deleted_email_index` (`deleted`,`email`),
  KEY `users_status_deleted_email_index` (`status`,`deleted`,`email`),
  KEY `users_status_email_index` (`status`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-21 17:43:33
