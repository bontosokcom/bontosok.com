import CookieParser from 'cookieparser'

import { ActionTree } from 'vuex'
import { RootState, State } from '~/types/store/index'

const axiosConfig = {
  withCredentials: true,
  origin: process.env.BASE_URL
};

export const state = (): State => ({
  //
});

export const actions: ActionTree<State, RootState> = {
  async nuxtServerInit (context, { req, app: { $axios } }) {
    if (req) {
      if (req && req.headers && req.headers.cookie) {
        const cookie = CookieParser.parse(req.headers.cookie);
        if (cookie.token) {
          $axios.setToken(cookie.token, 'Bearer');
          const loggedIn = await $axios.get(
            process.env.NGINX_URL + '/api/get-logged-in-user',
            axiosConfig
          )
            .then(function (response) {
              if (response.data.user) {
                context.commit('user/setUser', response.data.user);
                context.commit('auth/logIn');
                return true
              }
            })
            .catch(function () {
              context.commit('user/setUser', {});
              context.commit('auth/logOut');
              return false
            });

          if (loggedIn) {
            await $axios.get(
              process.env.NGINX_URL + '/api/car-catalog',
              axiosConfig
            )
              .then((response) => {
                context.commit('carCatalog/setCarCatalog', response.data)
              })
              .catch((error) => {
                console.log('error', process.env.NGINX_URL + '/api/car-catalog', error)
              })
          }
        }
      }
    }
  }
};
