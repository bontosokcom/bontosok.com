import { RootState } from '~/types/store'
import { CarCatalogState, Mutations, Actions, Getters } from '~/types/store/carCatalog'

export const state = (): CarCatalogState => <CarCatalogState>({
  carCatalog: [],
  make: '',
  model: '',
  platformYear: ''
});

export const mutations: Mutations<CarCatalogState> = {
  setCarCatalog(state, payload): void {
    state.carCatalog = payload
  },
  setMake(state, payload): void {
    state.make = payload
  },
  setModel(state, payload): void {
    state.model = payload
  },
  setPlatformYear(state, payload): void {
    state.platformYear = payload
  }
};

export const actions: Actions<CarCatalogState, RootState> = {
  async fetchCarCatalog (context) {
    if (context.state.carCatalog.length === 0) {
      (this as any).$axios.get(process.env.API_ENDPOINT + '/car-catalog')
        .then((response) => {
            context.commit('setCarCatalog', response.data)
        })
        .catch((error) => {
          console.log('error', process.env.NGINX_URL + '/api/car-catalog', error)
        })
    }
  }
};

export const getters: Getters<CarCatalogState, RootState> = {
  getMake: state => state.make,
  getModel: state => state.model,
  getPlatformYear: state => state.platformYear,
  getCarCatalog: state => state.carCatalog,
  getCarCatalogMakes: state => {
    return state.carCatalog.filter((catalog) => {
      return catalog.parent === '-'
    }).map((catalog) => {
      return {
        text: catalog.name,
        value: catalog.key
      }
    })
  },
  getCarCatalogModels: state => {
    return state.carCatalog.filter((catalog) => {
      return catalog.parent === state.make
    }).map((catalog) => {
      return {
        text: catalog.name,
        value: catalog.key
      }
    })
  },
  getCarCatalogPlatformYears: state => {
    return state.carCatalog.filter((catalog) => {
      return catalog.parent === state.model
    }).map((catalog) => {
      return {
        text: catalog.name,
        value: catalog.key
      }
    })
  }
};
