import { AuthState, Mutations, Getters} from '~/types/store/auth'
import { RootState } from '~/types/store'

export const state = (): AuthState => <AuthState>({
  isLoggedIn: false
});

export const mutations: Mutations<AuthState> = {
  logIn(state: AuthState): void {
    state.isLoggedIn = true
  },
  logOut(state: AuthState): void {
    state.isLoggedIn = false
  }
};

export const getters: Getters<AuthState, RootState> = {
  isLoggedIn: state => state.isLoggedIn
};
