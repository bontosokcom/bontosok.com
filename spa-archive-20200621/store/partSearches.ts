import { RootState } from '~/types/store'
import { PartSearchesState, Mutations, Actions, Getters } from '~/types/store/partSearches.d.ts'

export const state = (): PartSearchesState => <PartSearchesState>({
  partSearch: {},
  partSearches: [],
  partSearchesList: []
});

export const mutations: Mutations<PartSearchesState> = {
  setTitle (state, payload) {
    if (state.partSearch) {
      state.partSearch.title = payload
    }
  },
  setDescription (state, payload) {
    if (state.partSearch) {
      state.partSearch.description = payload
    }
  },
  setImages (state, payload) {
    if (state.partSearch) {
      state.partSearch.images = payload
    }
  },
  setPartSearch (state, payload) {
    state.partSearch = payload
  },
  setPartSearches (state, payload) {
    state.partSearches = payload
  },
  setPartSearchesList (state, payload) {
    state.partSearchesList = payload
  }
};

export const actions: Actions<PartSearchesState, RootState> = {
  async fetchPartSearches (context, callType) {
    const host = callType === 'SSR' ? process.env.NGINX_URL + '/api' : process.env.API_ENDPOINT
    await (this as any).$axios.get(host + '/get-part-searches')
      .then((response) => {
        context.commit('setPartSearches', response.data.part_searches)
      })
      .catch((error) => {
        console.log('ERROR - fetchPartSearches:', error)
        context.commit('setPartSearches', [])
      })
  },
  async fetchPartSearchesList (context, callType) {
    const host = callType === 'SSR' ? process.env.NGINX_URL + '/api' : process.env.API_ENDPOINT
    await (this as any).$axios.get(host + '/get-part-searches-list')
      .then((response) => {
        context.commit('setPartSearchesList', response.data.part_searches_list)
      })
      .catch((error) => {
        console.log('ERROR - fetchPartSearchesList:', error)
        context.commit('setPartSearchesList', [])
      })
  }
};

export const getters: Getters<PartSearchesState, RootState> = {
  getPartSearch: state => state.partSearch,
  getTitle: state => state.partSearch.title || '',
  getDescription: state => state.partSearch.description || '',
  getImages: state => state.partSearch.images || [],
  getPartSearches: state => state.partSearches,
  getPartSearchesList: state => state.partSearchesList
};
