import { RootState } from '~/types/store'
import { MyCarsState, Mutations, Actions, Getters } from '~/types/store/myCars'

export const state = (): MyCarsState => <MyCarsState>({
  myCar: null,
  myCars: [],
});

export const mutations: Mutations<MyCarsState> = {
  setMyCar (state, payload) {
    state.myCar = payload
  },
  setMyCars (state, payload) {
    state.myCars = payload
  }
};

export const actions: Actions<MyCarsState, RootState> = {
  async fetchMyCars (context, callType) {
    const host = callType === 'SSR' ? process.env.NGINX_URL + '/api' : process.env.API_ENDPOINT
    await (this as any).$axios.get(host + '/get-my-cars')
      .then((response) => {
        context.commit('setMyCars', response.data.my_cars)
      })
      .catch((error) => {
        console.log('ERROR - fetchMyCars:', error)
        context.commit('setMyCars', null)
      })
  }
};

export const getters: Getters<MyCarsState, RootState> = {
  getMyCar: state => state.myCar,
  getMyCars: state => state.myCars
};
