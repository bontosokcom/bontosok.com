import { RootState } from '~/types/store'
import { OfferRequestsState, Mutations, Actions, Getters } from '~/types/store/offerRequests'

export const state = (): OfferRequestsState => <OfferRequestsState>({
  offerRequests: []
});

export const mutations: Mutations<OfferRequestsState> = {
  setOfferRequests (state, payload) {
    state.offerRequests = payload
  }
};

export const actions: Actions<OfferRequestsState, RootState> = {
  async fetchOfferRequests (context) {
    await (this as any).$axios.get(process.env.API_ENDPOINT + '/get-offer-requests')
      .then((response) => {
        context.commit('setOfferRequests', response.data.offer_requests)
      })
      .catch(() => {
        context.commit('setOfferRequests', [])
      })
  }
};

export const getters: Getters<OfferRequestsState, RootState> = {
  getOfferRequests: state => state.offerRequests
};
