import { RootState } from '~/types/store'
import { OffersState, Mutations, Actions, Getters } from '~/types/store/offers'

export const state = (): OffersState => <OffersState>({
  offer: null,
  offers: []
});

export const mutations: Mutations<OffersState> = {
  setOffer (state, payload) {
    state.offer = payload
  },
  setOffers (state, payload) {
    state.offers = payload
  }
};

export const actions: Actions<OffersState, RootState> = {
  async fetchOffer (context) {
    await (this as any).$axios.get(process.env.API_ENDPOINT + '/get-offer')
      .then((response) => {
        context.commit('setOffer', response.data.offer)
      })
      .catch(() => {
        context.commit('setOffer', {})
      })
  },
  async fetchOffers (context) {
    await (this as any).$axios.get(process.env.API_ENDPOINT + '/get-offers')
      .then((response) => {
        context.commit('setOffers', response.data.offers)
      })
      .catch(() => {
        context.commit('setOffers', [])
      })
  }
};

export const getters: Getters<OffersState, RootState> = {
  getOffer: state => state.offer,
  getOffers: state => state.offers
};
