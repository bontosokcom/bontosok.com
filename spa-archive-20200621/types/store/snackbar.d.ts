import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'

export interface SnackbarContent {
  type: string
  text: string
  timeout?: number
  buttonText?: string
}

export interface SnackbarState {
  snackbar: boolean
  type: string
  text: string
  timeout: number
  buttonText: string
}

export interface Mutations<S> extends MutationTree<S> {
  setSnackbar (state: S, payload: boolean): void
  setType (state: S, payload: string): void
  setText (state: S, payload: string): void
  setTimeout (state: S, payload: number): void
  setButtonText (state: S, payload: string): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  showSnackbar (context: ActionContext<S, R>, payload: SnackbarContent): void
  hideSnackbar (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getSnackbar (state: S, getters: Getters<S, R>, rootState: R): boolean
  getType (state: S, getters: Getters<S, R>, rootState: R): string
  getText (state: S, getters: Getters<S, R>, rootState: R): string
  getTimeout (state: S, getters: Getters<S, R>, rootState: R): number
  getButtonText (state: S, getters: Getters<S, R>, rootState: R): string
}
