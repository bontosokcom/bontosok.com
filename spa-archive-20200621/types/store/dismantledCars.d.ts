import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { DismantledCarItem } from '~/types/models/dismantledCars'

export interface DismantledCarsState {
  dismantledCar: DismantledCarItem | null
  dismantledCarCode: string
  dismantledCars: DismantledCarItem[] | any[] | null
}

export interface Mutations<S> extends MutationTree<S> {
  setDismantledCar (state: S, payload: DismantledCarItem | null): void
  setDismantledCarCode (state: S, payload: string): void
  setDismantledCars (state: S, payload: DismantledCarItem[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchDismantledCars (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getDismantledCar (state: S, getters: Getters<S, R>, rootState: R): any
  getDismantledCarCode (state: S, getters: Getters<S, R>, rootState: R): string
  getDismantledCars (state: S, getters: Getters<S, R>, rootState: R): any
}
