import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { PartSearch, PartSearchItem } from '~/types/models/partSearches'

export interface PartSearchesState {
  partSearch: PartSearch
  partSearches: PartSearch[] | any[]
  partSearchesList: PartSearchItem[]
}

export interface Mutations<S> extends MutationTree<S> {
  setPartSearch (state: S, payload: PartSearch): void
  setTitle (state: S, payload: string): void
  setDescription (state: S, payload: string): void
  setImages (state: S, payload: any[]): void
  setPartSearches (state: S, payload: PartSearch[] | []): void
  setPartSearchesList (state: S, payload: PartSearchItem[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchPartSearches (context: ActionContext<S, R>, callType: String): void
  fetchPartSearchesList (context: ActionContext<S, R>, callType: String): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getPartSearch (state: S, getters: Getters<S, R>, rootState: R): PartSearch
  getTitle (state: S, getters: Getters<S, R>, rootState: R): string
  getDescription (state: S, getters: Getters<S, R>, rootState: R): string
  getImages (state: S, getters: Getters<S, R>, rootState: R): any[]
  getPartSearches (state: S, getters: Getters<S, R>, rootState: R): PartSearch[]
  getPartSearchesList (state: S, getters: Getters<S, R>, rootState: R): PartSearchItem[]
}
