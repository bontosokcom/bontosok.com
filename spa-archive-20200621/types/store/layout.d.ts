import { MutationTree, GetterTree } from 'vuex'

export interface LayoutState {
  title: string
}

export interface Mutations<S> extends MutationTree<S> {
  setTitle (state: S, payload: string): void,
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getTitle (state: S, getters: Getters<S, R>, rootState: R): string
}
