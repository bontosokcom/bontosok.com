import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { OfferRequestItem } from '~/types/models/offerRequests'

export interface OfferRequestsState {
  offerRequests: OfferRequestItem[] | any[]
}

export interface Mutations<S> extends MutationTree<S> {
  setOfferRequests (state: S, payload: OfferRequestItem[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchOfferRequests (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getOfferRequests (state: S, getters: Getters<S, R>, rootState: R): OfferRequestItem[]
}
