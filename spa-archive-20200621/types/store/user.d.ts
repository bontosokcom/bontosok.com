import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { UserItem, UserType } from '~/types/models/user'

export interface UserState {
  user: UserItem
}

export interface Mutations<S> extends MutationTree<S> {
  setUser (state: S, payload: UserItem): void
  setUsername (state: S, payload: string): void
  setFirstName (state: S, payload: string): void
  setLastName (state: S, payload: string): void
  setEmail (state: S, payload: string): void
  setType (state: S, payload: UserType): void
  setZip (state: S, payload: string): void
  setCity (state: S, payload: string): void
  setCompanyName (state: S, payload: string): void
  setPhone (state: S, payload: string): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchLoggedInUser (context: ActionContext<S, R>): void
  removeUser (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getUser (state: S, getters: Getters<S, R>, rootState: R): UserItem
  getUserType (state: S, getters: Getters<S, R>, rootState: R): UserType
  userIsFetched (state: S, getters: Getters<S, R>, rootState: R): boolean
}
