import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { MyCarItem } from '~/types/models/myCars'

export interface MyCarsState {
  myCar: MyCarItem | null
  myCars: MyCarItem[] | any[] | null
}

export interface Mutations<S> extends MutationTree<S> {
  setMyCar (state: S, payload: MyCarItem | null): void
  setMyCars (state: S, payload: MyCarItem[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchMyCars (context: ActionContext<S, R>, callType: String): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getMyCar (state: S, getters: Getters<S, R>, rootState: R): any
  getMyCars (state: S, getters: Getters<S, R>, rootState: R): any
}
