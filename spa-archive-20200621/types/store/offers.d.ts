import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { Offer } from '~/types/models/offers'

export interface OffersState {
  offer: Offer | null
  offers: Offer[]
}

export interface Mutations<S> extends MutationTree<S> {
  setOffer (state: S, payload: Offer | null): void
  setOffers (state: S, payload: Offer[]): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchOffer (context: ActionContext<S, R>): void
  fetchOffers (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getOffer (state: S, getters: Getters<S, R>, rootState: R): Offer | null
  getOffers (state: S, getters: Getters<S, R>, rootState: R): Offer[]
}
