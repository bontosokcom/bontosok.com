import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'

export interface CarCatalogState {
  carCatalog: any
  make: string
  model: string
  platformYear: string
}

export interface Mutations<S> extends MutationTree<S> {
  setCarCatalog (state: S, payload: []): void
  setMake (state: S, payload: ''): void
  setModel (state: S, payload: ''): void
  setPlatformYear (state: S, payload: ''): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchCarCatalog (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getMake (state: S, getters: Getters<S, R>, rootState: R): string
  getModel (state: S, getters: Getters<S, R>, rootState: R): string
  getPlatformYear (state: S, getters: Getters<S, R>, rootState: R): string
  getCarCatalog (state: S, getters: Getters<S, R>, rootState: R): []
  getCarCatalogMakes (state: S, getters: Getters<S, R>, rootState: R): []
  getCarCatalogModels (state: S, getters: Getters<S, R>, rootState: R): []
  getCarCatalogPlatformYears (state: S, getters: Getters<S, R>, rootState: R): []
}
