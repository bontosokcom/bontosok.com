import { StatusType } from '~/types/models/index'

export enum OfferRequestsStatus {
  new,
  archive,
  reject,
  progress,
  revoke
}

export interface OfferRequestPartSearch {
  id: number
  status: StatusType
  progressStatus: OfferRequestsStatus
  carCode: string
  title: string
  description: string
  createdAt: string
}

export interface OfferRequestItem {
  id?: number
  status?: StatusType
  code: string
  make?: string
  model?: string
  platform?: string
  year?: string
  order?: number
  partSearches: OfferRequestPartSearch[] | []
}
