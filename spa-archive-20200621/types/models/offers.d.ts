import {StatusType} from '~/types/models/index'

export interface Offer {
  id?: number
  dismantledCarsId: number
  partSearchesId: number
  offerRequestsStatus?: number
  status?: StatusType
  details?: string
  price?: number
  images?: string
  createdAt?: string
}
