import { CarItem } from '~/types/models/index'

export type DismantledCarItem = CarItem
