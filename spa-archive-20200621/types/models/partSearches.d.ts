import { StatusType } from '~/types/models/index'

export interface PartSearch {
  id?: number
  status?: StatusType
  carCode?: string
  title?: string
  description?: string
  images?: any[]
}

export interface PartSearchItem {
  id?: number
  status?: StatusType
  code: string
  make?: string
  model?: string
  platform?: string
  year?: string
  order?: number
  partSearches?: PartSearch[]
}
