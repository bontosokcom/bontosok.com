export enum UserType {
  offer,
  search
}

export interface UserItem {
  username?: string
  firstName?: string
  lastName?: string
  email: string
  type: UserType
  zip?: string
  city?: string
  companyName?: string
  phone?: string
}
