export enum StatusType {
  inactive,
  active
}

export interface CarItem {
  id?: number
  status?: StatusType
  code: string
  make?: string
  model?: string
  platform?: string
  year?: string
}
