interface routeOption {
  name?: string
  path: string
  component?: string
  redirect?: string
}

const routeOptions: routeOption[] = [
  {
    path: '/',
    component: 'pages/index.vue',
    name: 'home'
  },
  {
    path: '/look-for',
    component: 'pages/look-for.vue',
    name: 'look-for'
  },
  {
    path: '/offer',
    component: 'pages/offer.vue',
    name: 'offer'
  },
  {
    path: '/cookie',
    component: 'pages/cookie.vue',
    name: 'cookie'
  },
  {
    path: '/login',
    component: 'pages/auth/login.vue',
    name: 'login'
  },
  {
    path: '/registration',
    component: 'pages/user/registration.vue',
    name: 'registration'
  },
  {
    path: '/confirm-registration',
    component: 'pages/user/confirm-registration.vue',
    name: 'confirm-registration'
  },
  {
    path: '/forgot-password',
    component: 'pages/user/forgot-password.vue',
    name: 'forgot-password'
  },
  {
    path: '/reset-password',
    component: 'pages/user/reset-password.vue',
    name: 'reset-password'
  },
  {
    path: '/dashboard',
    component: 'pages/dashboard.vue',
    name: 'dashboard'
  },
  {
    path: '/profile',
    component: 'pages/user/profile.vue',
    name: 'profile'
  },
  {
    path: '/add-dismantled-car',
    component: 'pages/offer/add-dismantled-car.vue',
    name: 'add-dismantled-car'
  },
  {
    path: '/my-dismantled-cars',
    component: 'pages/offer/my-dismantled-cars.vue',
    name: 'my-dismantled-cars'
  },
  {
    path: '/offer/offer/:offersId',
    component: 'pages/offer/offer.vue',
    name: 'offer'
  },
  {
    path: '/offer-requests/:filter?',
    component: 'pages/offer/offer-requests.vue',
    name: 'offer-requests'
  },
  {
    path: '/pending-offers',
    component: 'pages/offer/pending-offers.vue',
    name: 'pending-offers'
  },
  {
    path: '/send-offer/:dismantledCarsId/:partSearchesId',
    component: 'pages/offer/send-offer.vue',
    name: 'send-offer'
  },
  {
    path: '/add-part-search',
    component: 'pages/search/add-part-search.vue',
    name: 'add-part-search'
  },
  {
    path: '/add-part-search-details/:myCarsId',
    component: 'pages/search/add-part-search-details.vue',
    name: 'add-part-search-details'
  },
  {
    path: '/edit-part-search/:myCarsId/:partSearchesId',
    component: 'pages/search/edit-part-search',
    name: 'edit-part-search'
  },
  {
    path: '/add-my-car',
    component: 'pages/search/add-my-car.vue',
    name: 'add-my-car'
  },
  {
    path: '/part-searches',
    component: 'pages/search/part-searches.vue',
    name: 'part-searches'
  },
  {
    path: '/offers',
    component: 'pages/search/offers.vue',
    name: 'offers'
  },
  {
    path: '/404',
    component: 'pages/404.vue',
    name: '404'
  }
];

const generateRoutes = (routes, resolve, __dirname) => {
  routeOptions.forEach((option: routeOption) => {
    const route: routeOption = {
      path: option.path
    };
    if (option.component) { route.component = resolve(__dirname, option.component) }
    if (option.name) { route.name = option.name }
    if (option.redirect) { route.redirect = option.redirect }
    routes.unshift(route)
  });
  const optionsLength: number = routeOptions.length;
  routes.splice(optionsLength, optionsLength)
};

export default generateRoutes
