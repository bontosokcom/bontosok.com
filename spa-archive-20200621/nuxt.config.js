import generateRoutes from './plugins/routes'
require('dotenv').config()

export default {
  mode: 'universal',
  /*
  ** Handle environment with Dotenv
  */
  env: {
    baseUrl: process.env.BASE_URL,
    API_ENDPOINT: process.env.API_ENDPOINT,
    NGINX_URL: process.env.NGINX_URL
  },
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#010101' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/sass/main.sass'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv'
  ],
  /*
  ** Router
  */
  router: {
    base: '/',
    extendRoutes (router, resolve) {
      generateRoutes(router, resolve, __dirname)
    }
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.API_ENDPOINT + '/'
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/sass/_variables.sass'],
    defaultAccess: {
      font: {
        family: 'Roboto'
      }
    },
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#38D173',
          secondary: '#FFFFFF',
          accent: '#3F51B5',
          default: '#8e8e8e',
          // info: colors.teal.lighten1,
          // warning: colors.amber.base,
          error: '#E13946',
          warning: '#424242'
          // success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
