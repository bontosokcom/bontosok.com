import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { PartSearch } from '~/types/models/partSearches'

export interface PartSearchesState {
  partSearch: PartSearch | null
  partSearches: PartSearch[] | []
}

export interface Mutations<S> extends MutationTree<S> {
  setPartSearch (state: S, payload: PartSearch | null): void
  setPartSearches (state: S, payload: PartSearch[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchPartSearches (context: ActionContext<S, R>, myCarsId: String): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getPartSearch (state: S, getters: Getters<S, R>, rootState: R): PartSearch | null
  getPartSearches (state: S, getters: Getters<S, R>, rootState: R): PartSearch[] | []
}
