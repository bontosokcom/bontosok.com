import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { DismantledCar } from '~/types/models/dismantledCars'

export interface DismantledCarsState {
  dismantledCar: DismantledCar | {}
  dismantledCarCode: string
  dismantledCars: DismantledCar[] | []
}

export interface Mutations<S> extends MutationTree<S> {
  setDismantledCar (state: S, payload: DismantledCar | {}): void
  setDismantledCarCode (state: S, payload: string): void
  setDismantledCars (state: S, payload: DismantledCar[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchDismantledCars (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getDismantledCar (state: S, getters: Getters<S, R>, rootState: R): DismantledCar | {}
  getDismantledCarCode (state: S, getters: Getters<S, R>, rootState: R): string
  getDismantledCars (state: S, getters: Getters<S, R>, rootState: R): DismantledCar[] | []
}
