import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { OfferRequest } from '~/types/models/offerRequests'

export interface OfferRequestsState {
  offerRequest: OfferRequest | {}
  offerRequests: OfferRequest[] | []
}

export interface Mutations<S> extends MutationTree<S> {
  setOfferRequest (state: S, payload: OfferRequest | {}): void
  setOfferRequests (state: S, payload: OfferRequest[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchOfferRequests (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getOfferRequest (state: S, getters: Getters<S, R>, rootState: R): OfferRequest | {}
  getOfferRequests (state: S, getters: Getters<S, R>, rootState: R): OfferRequest[] | []
}
