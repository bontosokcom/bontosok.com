import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { ReceivedOffer } from '~/types/models/receivedOffers'

export interface ReceivedOffersState {
  receivedOffer: ReceivedOffer | {}
  receivedOffers: ReceivedOffer[] | []
}

export interface Mutations<S> extends MutationTree<S> {
  setReceivedOffer (state: S, payload: ReceivedOffer | {}): void
  setReceivedOffers (state: S, payload: ReceivedOffer[] | []): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchReceivedOffers (context: ActionContext<S, R>): void
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getReceivedOffer (state: S, getters: Getters<S, R>, rootState: R): ReceivedOffer | {}
  getReceivedOffers (state: S, getters: Getters<S, R>, rootState: R): ReceivedOffer[] | []
}
