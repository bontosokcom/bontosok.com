import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'

export interface AuthState {
  isLoggedIn: boolean
}

export interface Mutations<S> extends MutationTree<S> {
  logIn (state: S): void,
  logOut(state: S): void
}

export interface Actions<S, R> extends ActionTree<S, R> {
  logOutUser (context: ActionContext<S, R>): void,
}

export interface Getters<S, R> extends GetterTree<S, R> {
  isLoggedIn (state: S, getters: Getters<S, R>, rootState: R): boolean
}
