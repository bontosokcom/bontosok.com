import { StatusType } from '~/types/models/index'
import { PartSearch } from '~/types/models/partSearches'
import { DismantledCar } from "~/types/models/dismantledCars";

export enum OfferRequestsStatus {
  archived = 'archived',
  arrived = 'arrived',
  closed = 'closed',
  declined = 'declined',
  progress = 'progress',
  revoked = 'revoked',
  succeed = 'succeed'
}

export interface OfferRequest {
  carCode?: string
  dismantledCarsId?: number
  dismantledCar: DismantledCar
  partSearchesId?: number
  status?: StatusType
  history?: string
  partSearch: PartSearch
}
