export enum StatusType {
  inactive = 'inactive',
  active = 'active'
}

export interface Car {
  id?: number
  status?: StatusType
  carCode: string
  make?: string
  model?: string
  platform?: string
  year?: string
}
