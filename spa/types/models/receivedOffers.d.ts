import { PartSearch } from '~/types/models/partSearches'
import { DismantledCar } from '~/types/models/dismantledCars'
import { Offer } from '~/types/models/offers'

export enum ReceivedOffersStatus {
  archived = 'archived',
  arrived = 'arrived',
  closed = 'closed',
  succeed = 'succeed'
}

export interface ReceivedOffer {
  carCode?: string
  dismantledCarsId?: number
  dismantledCar?: DismantledCar
  partSearchesId?: number
  partSearch?: PartSearch
  offerRequestsId?: number
  offersId?: number
  offers: Offer
  status?: ReceivedOffersStatus
  history?: string
}
