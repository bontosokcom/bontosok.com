export enum UserType {
  supply,
  demand
}

export interface User {
  username?: string
  firstName?: string
  lastName?: string
  isFilled?: string
  email: string
  type: UserType
  zip?: string
  city?: string
  companyName?: string
  phone?: string
}
