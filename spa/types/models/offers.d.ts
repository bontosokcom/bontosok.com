import {StatusType} from '~/types/models/index'

export interface Offer {
  id?: number
  dismantledCarsId: number
  partSearchesId: number
  offerRequestsId?: number
  carCode?: string
  status?: StatusType
  details?: string
  price?: number
  currency?: string
  images?: string
  createdAt?: string
}
