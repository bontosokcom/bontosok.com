import { StatusType } from '~/types/models/index'

export interface PartSearch {
  id?: number
  myCarsId: number
  status?: StatusType
  carCode?: string
  title?: string
  description?: string
  images?: any[]
  createdAt?: string
}
