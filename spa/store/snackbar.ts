import { RootState } from '~/types/store'
import { SnackbarState, Mutations, Actions, Getters } from '~/types/store/snackbar'

export const state = (): SnackbarState => ({
  snackbar: false,
  type: 'info',
  text: '',
  timeout: 5000,
  buttonText: 'Bezár'
});

export const mutations: Mutations<SnackbarState> = {
  setSnackbar (state, payload) {
    state.snackbar = payload
  },
  setType (state, payload) {
    state.type = payload
  },
  setText (state, payload) {
    state.text = payload
  },
  setTimeout (state, payload) {
    state.timeout = payload
  },
  setButtonText (state, payload) {
    state.buttonText = payload
  }
};

export const actions: Actions<SnackbarState, RootState> = {
  showSnackbar ({ commit }, payload) {
    commit('setSnackbar', true);
    commit('setType', payload.type);
    commit('setText', payload.text);
    if (payload.timeout) {
      commit('setTimeout', payload.timeout)
    }
    if (payload.buttonText) {
      commit('setButtonText', payload.buttonText)
    }
  },
  hideSnackbar ({ commit }) {
    commit('setSnackbar', false);
    commit('setType', 'info');
    commit('setText', '')
  }
};

export const getters: Getters<SnackbarState, RootState> = {
  getSnackbar: (state) => { return state.snackbar },
  getType: (state) => { return state.type },
  getText: (state) => { return state.text },
  getTimeout: (state) => { return state.timeout },
  getButtonText: (state) => { return state.buttonText }
};
