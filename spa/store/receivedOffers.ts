import { RootState } from '~/types/store'
import { ReceivedOffersState, Mutations, Actions, Getters } from '~/types/store/receivedOffers.d.ts'

export const state = (): ReceivedOffersState => <ReceivedOffersState>({
  receivedOffer: {},
  receivedOffers: []
});

export const mutations: Mutations<ReceivedOffersState> = {
  setReceivedOffer (state, payload) {
    state.receivedOffer = payload || {}
  },
  setReceivedOffers (state, payload) {
    state.receivedOffers = payload || []
  }
};

export const actions: Actions<ReceivedOffersState, RootState> = {
  async fetchReceivedOffers (context) {
    await (this as any).$axios.get(process.env.API_ENDPOINT + '/get-received-offers')
      .then((response) => {
        context.commit('setReceivedOffers', response.data.receivedOffers)
      })
      .catch(() => {
        context.commit('setReceivedOffers', [])
      })
  }
};

export const getters: Getters<ReceivedOffersState, RootState> = {
  getReceivedOffer: state => state.receivedOffer,
  getReceivedOffers: state => state.receivedOffers,
};
