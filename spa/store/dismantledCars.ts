import { RootState } from '~/types/store'
import { DismantledCarsState, Mutations, Actions, Getters } from '~/types/store/dismantledCars'

export const state = (): DismantledCarsState => <DismantledCarsState>({
  dismantledCar: {},
  dismantledCarCode: '',
  dismantledCars: []
});

export const mutations: Mutations<DismantledCarsState> = {
  setDismantledCar (state, payload) {
    state.dismantledCar = payload
  },
  setDismantledCarCode (state, payload) {
    state.dismantledCarCode = payload
  },
  setDismantledCars (state, payload) {
    state.dismantledCars = payload
  }
};

export const actions: Actions<DismantledCarsState, RootState> = {
  async fetchDismantledCars (context) {
    await (this as any).$axios.get(process.env.API_ENDPOINT + '/get-dismantled-cars')
      .then((response) => {
        context.commit('setDismantledCars', response.data.dismantled_cars)
      })
      .catch(() => {
        context.commit('setDismantledCars', null)
      })
  }
};

export const getters: Getters<DismantledCarsState, RootState> = {
  getDismantledCar: state => state.dismantledCar,
  getDismantledCarCode: state => state.dismantledCarCode,
  getDismantledCars: state => state.dismantledCars,
  getActiveDismantledCars: state => state.dismantledCars ? state.dismantledCars.filter(dismantledCar => dismantledCar.status === 'active') : []
};
