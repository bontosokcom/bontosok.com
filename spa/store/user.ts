import { RootState } from '~/types/store'
import { UserState, Mutations, Actions, Getters } from '~/types/store/user'

const Cookie = process.client ? require('js-cookie') : undefined;

export const state = (): UserState => <UserState>({
  user: {}
});

export const mutations: Mutations<UserState> = {
  setUser (state, payload) {
    state.user = payload
  },
  setType (state, payload) {
    if (state.user) {
      state.user.type = payload
    }
  },
  setEmail (state, payload) {
    if (state.user) {
      state.user.email = payload
    }
  },
  setCity (state, payload) {
    if (state.user) {
      state.user.city = payload
    }
  },
  setCompanyName (state, payload) {
    if (state.user) {
      state.user.companyName = payload
    }
  },
  setFirstName (state, payload) {
    if (state.user) {
      state.user.firstName = payload
    }
  },
  setLastName (state, payload) {
    if (state.user) {
      state.user.lastName = payload
    }
  },
  setPhone (state, payload) {
    if (state.user) {
      state.user.phone = payload
    }
  },
  setUsername (state, payload) {
    if (state.user) {
      state.user.username = payload
    }
  },
  setZip (state, payload) {
    if (state.user) {
      state.user.zip = payload
    }
  },
};

export const actions: Actions<UserState, RootState> = {
  async fetchLoggedInUser (context) {
    (this as any).$axios.get(process.env.API_ENDPOINT + '/get-logged-in-user')
      .then((response) => {
        context.commit('setUser', response.data.user)
      })
      .catch(() => {
        context.commit('auth/logOut');
        context.commit('setUser', {});
        Cookie.remove('token')
      })
  },
  removeUser: ({commit}) => { commit('setUser', {})}
};

export const getters: Getters<UserState, RootState> = {
  getUser: state => state.user,
  getUserType: state => state.user.type,
  userIsFetched: state => !!state.user.email,
  isFilled: state => state.user.isFilled === '1'
};
