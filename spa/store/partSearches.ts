import { RootState } from '~/types/store'
import { PartSearchesState, Mutations, Actions, Getters } from '~/types/store/partSearches.d.ts'

export const state = (): PartSearchesState => <PartSearchesState>({
  partSearch: null,
  partSearches: []
});

export const mutations: Mutations<PartSearchesState> = {
  setPartSearch (state, payload) {
    state.partSearch = payload
  },
  setPartSearches (state, payload) {
    state.partSearches = payload
  }
};

export const actions: Actions<PartSearchesState, RootState> = {
  async fetchPartSearches (context, myCarsId) {
    await (this as any).$axios.get(process.env.API_ENDPOINT + '/get-part-searches', {
      params: {
        my_cars_id: myCarsId
      }
    })
      .then((response) => {
        context.commit('setPartSearches', response.data.part_searches)
      })
      .catch((error) => {
        console.log('ERROR - fetchPartSearches:', error)
        context.commit('setPartSearches', [])
      })
  }
};

export const getters: Getters<PartSearchesState, RootState> = {
  getPartSearch: state => state.partSearch,
  getPartSearches: state => state.partSearches
};
