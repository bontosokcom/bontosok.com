import { RootState } from '~/types/store'
import { LayoutState, Getters, Mutations } from '~/types/store/layout'

export const state = (): LayoutState => <LayoutState>({
  title: ''
});

export const mutations: Mutations<LayoutState> = {
  setTitle (state, payload) {
    state.title = payload
  }
};

export const getters: Getters<LayoutState, RootState> = {
  getTitle (state) { return state.title }
};
