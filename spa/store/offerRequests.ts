import { RootState } from '~/types/store'
import { OfferRequestsState, Mutations, Actions, Getters } from '~/types/store/offerRequests.d.ts'

export const state = (): OfferRequestsState => <OfferRequestsState>({
  offerRequest: {},
  offerRequests: []
});

export const mutations: Mutations<OfferRequestsState> = {
  setOfferRequest (state, payload) {
    state.offerRequest = payload || {}
  },
  setOfferRequests (state, payload) {
    state.offerRequests = payload || []
  }
};

export const actions: Actions<OfferRequestsState, RootState> = {
  async fetchOfferRequests (context) {
    await (this as any).$axios.get(process.env.API_ENDPOINT + '/get-offer-requests')
      .then((response) => {
        context.commit('setOfferRequests', response.data.offerRequests)
      })
      .catch(() => {
        context.commit('setOfferRequests', [])
      })
  }
};

export const getters: Getters<OfferRequestsState, RootState> = {
  getOfferRequest: state => state.offerRequest,
  getOfferRequests: state => state.offerRequests,
};
