interface routeOption {
  name?: string
  path: string
  component?: string
  redirect?: string
}

const routeOptions: routeOption[] = [
  {
    path: '/',
    component: 'pages/index.vue',
    name: 'home'
  },
  {
    path: '/supply',
    component: 'pages/supply.vue',
    name: 'supply'
  },
  {
    path: '/cookie',
    component: 'pages/cookie.vue',
    name: 'cookie'
  },
  {
    path: '/login',
    component: 'pages/auth/login.vue',
    name: 'login'
  },
  {
    path: '/registration',
    component: 'pages/user/registration.vue',
    name: 'registration'
  },
  {
    path: '/confirm-registration',
    component: 'pages/user/confirm-registration.vue',
    name: 'confirm-registration'
  },
  {
    path: '/forgot-password',
    component: 'pages/user/forgot-password.vue',
    name: 'forgot-password'
  },
  {
    path: '/reset-password',
    component: 'pages/user/reset-password.vue',
    name: 'reset-password'
  },
  {
    path: '/dashboard',
    component: 'pages/dashboard.vue',
    name: 'dashboard'
  },
  {
    path: '/profile',
    component: 'pages/user/profile.vue',
    name: 'profile'
  },
  {
    path: '/add-dismantled-car',
    component: 'pages/supply/add-dismantled-car.vue',
    name: 'add-dismantled-car'
  },
  {
    path: '/my-dismantled-cars',
    component: 'pages/supply/my-dismantled-cars.vue',
    name: 'my-dismantled-cars'
  },
  {
    path: '/offer-requests',
    component: 'pages/supply/offer-requests.vue',
    name: 'offer-requests'
  },
  {
    path: '/offer-requests/:dismantledCarsId',
    component: 'pages/supply/offer-requests-list.vue',
    name: 'offer-requests-list'
  },
  {
    path: '/pending-offers',
    component: 'pages/supply/pending-offers.vue',
    name: 'pending-offers'
  },
  {
    path: '/pending-offers/:dismantledCarsId',
    component: 'pages/supply/pending-offers-list.vue',
    name: 'pending-offers-list'
  },
  {
    path: '/add-my-car',
    component: 'pages/demand/add-my-car.vue',
    name: 'add-my-car'
  },
  {
    path: '/my-cars',
    component: 'pages/demand/my-cars.vue',
    name: 'my-cars'
  },
  {
    path: '/start-part-search/:myCarsId',
    component: 'pages/demand/start-part-search.vue',
    name: 'start-part-search'
  },
  {
    path: '/start-part-search',
    component: 'pages/demand/part-searches.vue',
    name: 'part-searches-alias'
  },
  {
    path: '/part-searches',
    component: 'pages/demand/part-searches.vue',
    name: 'part-searches'
  },
  {
    path: '/part-searches/:myCarsId',
    component: 'pages/demand/part-searches-list.vue',
    name: 'part-searches-list'
  },
  {
    path: '/received-offers',
    component: 'pages/demand/received-offers.vue',
    name: 'received-offers'
  },
  {
    path: '/received-offers/:myCarsId',
    component: 'pages/demand/received-offers-list.vue',
    name: 'received-offers-list'
  },
  {
    path: '/send-offer/:partSearchesId',
    component: 'pages/supply/send-offer.vue',
    name: 'send-offer'
  },
  {
    path: '/offer',
    component: 'pages/supply/offer.vue',
    name: 'offer'
  },
  {
    path: '/404',
    component: 'pages/404.vue',
    name: '404'
  }
];

const generateRoutes = (routes, resolve, __dirname) => {
  routeOptions.forEach((option: routeOption) => {
    const route: routeOption = {
      path: option.path
    };
    if (option.component) { route.component = resolve(__dirname, option.component) }
    if (option.name) { route.name = option.name }
    if (option.redirect) { route.redirect = option.redirect }
    routes.unshift(route)
  });
  const optionsLength: number = routeOptions.length;
  routes.splice(optionsLength, optionsLength)
};

export default generateRoutes
