const Cookie = process.client ? require('js-cookie') : undefined

export default function setAxiosToken (context) {
  if (Cookie) {
    const token = Cookie.get('token')
    context.$axios.setToken(token, 'Bearer')
  }
}
